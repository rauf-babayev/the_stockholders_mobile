import {StyleSheet} from 'react-native';
import {dimensions} from '../../services/utils/constants';

export const styles = StyleSheet.create({
  mainContainer: {
    width: dimensions.COMMENT_CONTAINER_W,
    height: dimensions.COMMENT_CONTAINER_H,
    flexDirection: 'row',
    marginBottom: dimensions.COMMENT_ITEM_MARGIN_BOTTOM,
    padding: dimensions.COMMENT_ITEM_PADDING,
    alignItems: 'center',
  },
  commentText: {
    fontSize: dimensions.APP_MAIN_FONT_SIZE - 4,
    flex: 1,
  },
  anonymousIconContainer: {
    height: dimensions.COMMENT_ITEM_ICON_CONTAINER_HEIGHT,
    marginRight: dimensions.COMMENT_ITEM_ICON_MARGIN_R,
    justifyContent: 'center',
    alignItems: 'center',
    padding: dimensions.MAIN_SCREEN_USER_PICTURE_PADDING,
  },
});

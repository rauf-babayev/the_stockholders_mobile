import React from 'react';
import {ImageBackground, ScrollView} from 'react-native';
import {styles} from './styles';
import PassRecoveryForm from '../../components/passRecoveryForm';
import {images} from './../../src/img/index';
import Header from './../../components/header/index';
import {useNavigation, CommonActions} from '@react-navigation/native';
import {useDispatch} from 'react-redux';
import {navigateToScreenAction} from '../../modules/redux/actions';
import {strings} from '../../services/utils/strings';

const PassRecoveryScreen = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const onPressMethod = () => {
    dispatch(
      navigateToScreenAction(() => {
        navigation.dispatch(
          CommonActions.reset({
            routes: [{name: strings.routes.AUTH_STACK_LOGIN_ROUTE_NAME}],
          }),
        );
      }),
    );
  };

  return (
    <ImageBackground style={styles.mainContainer} source={images.MAIN_BG}>
      <Header
        onPressText={strings.common.HEADER_CLOSE_BTN_TEXT}
        onPressMethod={onPressMethod}
      />
      <ScrollView
        style={styles.scroll}
        contentContainerStyle={styles.scrollContent}>
        <PassRecoveryForm />
      </ScrollView>
    </ImageBackground>
  );
};

export default PassRecoveryScreen;

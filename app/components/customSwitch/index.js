/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {TouchableWithoutFeedback, View} from 'react-native';
import {BWIconSun, BWIconMoon} from '../../src/img/svgImg';
import {styles} from './styles';
import {dimensions} from '../../services/utils/constants';
import {colors} from '../../services/utils/colors';
import {strings} from '../../services/utils/strings';

const CustomSwitch = ({isDark, changeTheme, theme}) => {
  return (
    <TouchableWithoutFeedback
      onPress={() => {
        changeTheme();
      }}>
      <View
        style={[
          styles.backContainer,
          {
            backgroundColor:
              colors[
                `${
                  theme == strings.common.MAIN_APP_LIGHT_THEME
                    ? strings.common.APP_DARK_THEME_TEXT
                    : strings.common.APP_LIGHT_THEME_TEXT
                }`
              ].AUTH_FORM_BTN_BG,
          },
        ]}>
        <View
          style={[
            styles.circleContainer,
            {
              backgroundColor: colors[`${theme}`].AUTH_FORM_BTN_BG,
              left: !isDark ? 0 : null,
              right: !isDark ? null : 0,
            },
          ]}>
          {!isDark ? (
            <BWIconSun
              iconWH={dimensions.CUSTOM_SWITCH_SUN_ICON_WH}
              theme={theme}
            />
          ) : (
            <BWIconMoon
              iconWH={dimensions.CUSTOM_SWITCH_MOON_ICON_WH}
              theme={theme}
            />
          )}
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default CustomSwitch;

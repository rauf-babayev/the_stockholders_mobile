import React from 'react';
import {
  ImageBackground,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
} from 'react-native';
import {styles} from './styles';
import AuthForm from '../../components/authForm';
import {images} from './../../src/img/index';
import Header from './../../components/header/index';
import CustomLoader from '../../components/customLoader';
import {useSelector} from 'react-redux';
import {strings} from '../../services/utils/strings';

const AuthScreen = () => {
  const isLoading = useSelector(state => state.appReducer.isLoading);

  return (
    <KeyboardAvoidingView
      behavior={
        Platform.OS === strings.common.KEYBOARD_AVOIDING_OS
          ? strings.common.KEYBOARD_AVOIDING_BEHAVIOR
          : null
      }
      style={styles.container}>
      <ImageBackground style={styles.mainContainer} source={images.MAIN_BG}>
        <Header />
        {isLoading ? <CustomLoader /> : null}
        <ScrollView
          style={styles.scroll}
          contentContainerStyle={styles.scrollContent}>
          <AuthForm />
        </ScrollView>
      </ImageBackground>
    </KeyboardAvoidingView>
  );
};

export default AuthScreen;

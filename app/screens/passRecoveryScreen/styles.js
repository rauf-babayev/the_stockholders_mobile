import {StyleSheet} from 'react-native';
import {dimensions} from '../../services/utils/constants';

export const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  scroll: {
    width: dimensions.COMMENT_ITEM_SCROLL_WIDTH,
    height: dimensions.COMMENT_ITEM_SCROLL_WIDTH,
    marginTop: dimensions.HEADER_CONTAINER_H,
  },
  scrollContent: {
    justifyContent: 'center',
    alignItems: 'center',
    flexGrow: 1,
  },
});

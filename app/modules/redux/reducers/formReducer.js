import {strings} from '../../../services/utils/strings';

let initialState = {
  isUserAuthorized: false,
  userInfo: '',
  userComments: [],
  newComments: false,
};

export const formReducer = (state = initialState, action) => {
  switch (action.type) {
    case strings.actionTypes.SET_AUTH_USER:
      return {
        ...state,
        isUserAuthorized: true,
        userInfo: action.data,
        userComments: action.data.publicComments,
      };
    case strings.actionTypes.SET_LOGOUT_USER:
      return {
        ...state,
        isUserAuthorized: false,
        userInfo: '',
        userComments: [],
        newComments: false,
      };
    case strings.actionTypes.SET_USER_COMMENTS:
      return {
        ...state,
        userComments:
          action.data.publicComments[0] == '' ? [] : action.data.publicComments,
        newComments:
          action.data.publicComments.length <= state.userComments.length
            ? false
            : true,
        userInfo: action.data,
      };
    case strings.actionTypes.SET_NEW_COMMENTS:
      return {
        ...state,
        newComments: false,
      };
    default:
      return state;
  }
};

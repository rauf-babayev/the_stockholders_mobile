import {StyleSheet} from 'react-native';
import {dimensions} from '../../services/utils/constants';

export const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    position: 'absolute',
    top: 0,
    width: dimensions.HEADER_CONTAINER_W,
    height: dimensions.HEADER_CONTAINER_H,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: dimensions.HEADER_CONTAINER_PADDING_H,
  },
  switchContainer: {
    flex: 0.3,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  logoutBtnContainer: {
    flex: 0.3,
    alignItems: 'flex-start',
  },
  logoutText: {
    fontSize: dimensions.APP_MAIN_FONT_SIZE,
  },
  switchBtn: {},
  logoutBtn: {},
});

import React, {useState, useRef, useEffect} from 'react';
import {Text, TouchableOpacity, Keyboard, View} from 'react-native';
import CustomTextInput from '../customTextInput';
import {styles} from './styles';
import {strings} from './../../services/utils/strings';
import {useDispatch, useSelector} from 'react-redux';
import {colors} from './../../services/utils/colors';
import {submitPassRecoveryAction} from './../../modules/redux/actions/index';
import Animated, {Easing} from 'react-native-reanimated';
import Toast from 'react-native-toast-message';
import {dimensions} from '../../services/utils/constants';

const PassRecoveryForm = () => {
  const dispatch = useDispatch();
  const theme = useSelector(state => state.appReducer.appTheme);
  const opacityValue = useRef(new Animated.Value(0)).current;
  const [passRecoveryState, setPassRecoveryState] = useState({
    login: '',
    loginError: false,
  });

  const toastConfig = {
    info: internalState => (
      <View
        style={[
          styles.toast,
          {
            backgroundColor: colors[`${theme}`].AUTH_FORM_BG,
            borderColor: colors[`${theme}`].AUTH_FORM_BTN_BG,
          },
        ]}>
        <Text
          style={{
            fontSize: dimensions.APP_MAIN_FONT_SIZE - 2,
            color: colors[`${theme}`].APP_COLOR,
          }}>
          {internalState.text1}
        </Text>
        <Text
          style={{
            fontSize: dimensions.APP_MAIN_FONT_SIZE - 6,
            color: colors[`${theme}`].APP_COLOR,
          }}>
          {internalState.text2}
        </Text>
      </View>
    ),
  };

  useEffect(() => {
    Animated.timing(opacityValue, {
      toValue: 1,
      duration: 500,
      useNativeDriver: true,
      easing: Easing.linear,
    }).start();
  }, []);

  const submitPassRecovery = () => {
    Keyboard.dismiss();
    dispatch(
      submitPassRecoveryAction(passRecoveryState.login, setPassRecoveryState),
    );
  };

  return (
    <>
      <Toast config={toastConfig} ref={ref => Toast.setRef(ref)} />
      <Animated.View
        style={[
          styles.formContainer,
          {
            backgroundColor: colors[`${theme}`].AUTH_FORM_BG,
            opacity: opacityValue,
          },
        ]}>
        <Text style={[styles.formText, {color: colors[`${theme}`].APP_COLOR}]}>
          {strings.common.PASS_RECOVERY_SCREEN_TEXT}
        </Text>
        <CustomTextInput
          testID={strings.testIds.PASS_RECOVERY_FORM_INPUT}
          placeholder={strings.common.AUTH_FORM_USERNAME_PLACEHOLDER}
          formState={passRecoveryState}
          setFormState={setPassRecoveryState}
          label={''}
        />
        <TouchableOpacity
          testID={strings.testIds.PASS_RECOVERY_FORM_SUBMIT_BTN}
          style={[
            styles.btnContainer,
            {backgroundColor: colors[`${theme}`].AUTH_FORM_BTN_BG},
          ]}
          onPress={submitPassRecovery}>
          <Text style={[styles.btnText, {color: colors[`${theme}`].APP_COLOR}]}>
            {strings.common.PASS_RECOVERY_SCREEN_BTN_TEXT}
          </Text>
        </TouchableOpacity>
      </Animated.View>
    </>
  );
};

export default PassRecoveryForm;

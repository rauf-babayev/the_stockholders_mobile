import {StyleSheet} from 'react-native';
import {dimensions} from '../../services/utils/constants';

export const styles = StyleSheet.create({
  backContainer: {
    height: dimensions.CUSTOM_SWITCH_BG_H,
    borderRadius: dimensions.CUSTOM_SWITCH_BORDER_RADIUS,
    width: dimensions.CUSTOM_SWITCH_BG_W,
  },
  circleContainer: {
    width: dimensions.CUSTOM_SWITCH_ICON_WH,
    height: dimensions.CUSTOM_SWITCH_ICON_WH,
    top: dimensions.CUSTOM_SWITCH_ICON_MARGIN_TOP,
    borderRadius: dimensions.CUSTOM_SWITCH_BORDER_RADIUS,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
  },
});

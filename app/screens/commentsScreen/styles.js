import {StyleSheet} from 'react-native';
import {dimensions} from '../../services/utils/constants';

export const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    paddingTop: dimensions.COMMENTS_SCREEN_PADDING_TOP,
    justifyContent: 'center',
    alignItems: 'center',
  },
  commentsContainer: {
    width: dimensions.COMMENTS_SCREEN_CONTAINER_WH,
    height: dimensions.COMMENTS_SCREEN_CONTAINER_WH,
    marginVertical: dimensions.COMMENTS_SCREEN_CONTAINER_MARGIN_V,
  },
  commentsScroll: {
    alignItems: 'center',
    paddingTop: dimensions.COMMENTS_SCREEN_SCROLL_PADDINGS,
    paddingHorizontal: dimensions.COMMENTS_SCREEN_SCROLL_PADDINGS,
  },
  noComment: {
    maxWidth: '100%',
    fontSize: dimensions.APP_MAIN_FONT_SIZE,
    textAlign: 'center',
  },
});

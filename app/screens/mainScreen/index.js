import React, {useEffect, useRef, useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  Image,
  Animated,
  Easing,
  RefreshControl,
} from 'react-native';
import CustomRow from '../../components/customRow';
import {styles} from './styles';
import {
  DefaultSvgIcon,
  AnonymousUserIcon,
  CameraIconSvg,
  SvgErrorImg,
} from './../../src/img/svgImg';
import {svgPaths} from './../../services/utils/svgPaths';
import {useDispatch, useSelector} from 'react-redux';
import {useNavigation, CommonActions} from '@react-navigation/native';
import {
  navigateToScreenAction,
  logoutUserAction,
  setProfilePictureAction,
  deleteProfilePictureAction,
  fetchUserCommentsAction,
  setNewCommentsAction,
} from '../../modules/redux/actions';
import {images} from './../../src/img/index';
import Header from './../../components/header/index';
import {strings} from '../../services/utils/strings';
import {colors} from './../../services/utils/colors';
import {dimensions} from '../../services/utils/constants';
import ImagePicker from 'react-native-image-picker';
import ModalPrompt from '../../components/modalPrompt';
import Toast from 'react-native-toast-message';
import ModalComment from '../../components/modalComment';
import {fieldNames, serverResponse} from '../../../mocks';

const MainScreen = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const theme = useSelector(state => state.appReducer.appTheme);
  const userInfo = useSelector(state => state.formReducer.userInfo);
  const userComments = useSelector(state => state.formReducer.userComments);
  const newComments = useSelector(state => state.formReducer.newComments);
  const isRefreshing = useSelector(state => state.appReducer.isRefreshing);
  const userLog = useSelector(state =>
    state.profileReducer.accounts.filter(item => {
      if (item.login == userInfo.account.login) {
        return item.log;
      }
    }),
  );
  const profilePicture = useSelector(state =>
    state.profileReducer.accounts.filter(item => {
      if (item.login == userInfo.account.login) {
        return item.image;
      }
    }),
  );
  const [isPrompt, setIsPrompt] = useState(false);
  const goToCommentsScreen = () => {
    clearInterval(idTime);
    dispatch(
      navigateToScreenAction(() => {
        navigation.dispatch(
          CommonActions.reset({
            routes: [{name: strings.routes.MAIN_STACK_COMMENTS_SCREEN_ROUTE}],
          }),
        );
      }),
    );
  };
  const [idTime, setIdTime] = useState('');

  useEffect(() => {
    Animated.parallel([
      Animated.timing(opacityValue, {
        toValue: 1,
        duration: 500,
        useNativeDriver: true,
        easing: Easing.linear,
      }),
      Animated.timing(transformValue, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
        easing: Easing.linear,
      }),
    ]).start();

    const identificator = setInterval(() => {
      dispatch(
        fetchUserCommentsAction(
          userInfo.account.login,
          userInfo.account.password,
        ),
      );
    }, 10000);
    setIdTime(identificator);
  }, []);

  const onPressLogout = () => {
    clearInterval(idTime);
    dispatch(logoutUserAction());
  };

  const selectPictureFromCamera = () => {
    ImagePicker.launchCamera(
      {
        tintColor: 'black',
        quality: 1.0,
        maxWidth: 500,
        maxHeight: 500,
      },
      response => {
        if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.didCancel) {
          console.log('Response got cancelled');
        } else {
          dispatch(
            setProfilePictureAction(response.uri, userInfo.account.login),
          );
        }
      },
    );
  };

  const selectPictureFromGalleryBtn = () => {
    ImagePicker.launchImageLibrary(
      {
        tintColor: 'black',
        quality: 1.0,
        maxWidth: 500,
        maxHeight: 500,
      },
      response => {
        if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.didCancel) {
          console.log('Response got cancelled');
        } else {
          dispatch(
            setProfilePictureAction(response.uri, userInfo.account.login),
          );
        }
      },
    );
  };

  useEffect(() => {
    if (newComments) {
      Toast.show({
        type: strings.common.PASS_RECOVERY_TOAST_TYPE,
        position: strings.common.PASS_RECOVERY_TOAST_POSITION,
        text1: strings.common.NEW_COMMENT_TOAST_FRIST_TEXT,
        text2: strings.common.NEW_COMMENT_TOAST_SECOND_TEXT,
        visibilityTime: dimensions.PASS_RECOVERY_TOAST_VISIBILITY_TIME,
        autoHide: true,
        topOffset: dimensions.PASS_RECOVERY_TOAST_TOP_OFFSET,
        bottomOffset: dimensions.PASS_RECOVERY_TOAST_BOTTOM_OFFSET,
      });
      dispatch(setNewCommentsAction(false));
    }
  }, [userComments]);

  const deleteProfilePictureBtn = () => {
    dispatch(deleteProfilePictureAction(userInfo.account.login));
    setIsPrompt(false);
  };

  const opacityValue = useRef(new Animated.Value(0)).current;
  const transformValue = useRef(new Animated.Value(-40)).current;

  const toastConfig = {
    info: internalState => (
      <View
        style={[
          styles.toast,
          {
            backgroundColor: colors[`${theme}`].AUTH_FORM_BG,
            borderColor: colors[`${theme}`].AUTH_FORM_BTN_BG,
          },
        ]}>
        <Text
          style={{
            fontSize: dimensions.APP_MAIN_FONT_SIZE - 2,
            color: colors[`${theme}`].APP_COLOR,
          }}>
          {internalState.text1}
        </Text>
        <Text
          style={{
            fontSize: dimensions.APP_MAIN_FONT_SIZE - 6,
            color: colors[`${theme}`].APP_COLOR,
          }}>
          {internalState.text2}
        </Text>
      </View>
    ),
  };

  const refreshComments = () => {
    dispatch(
      fetchUserCommentsAction(
        userInfo.account.login,
        userInfo.account.password,
      ),
    );
  };

  const [logState, setLogState] = useState({
    logText: [],
    showLog: false,
  });

  return (
    <ImageBackground style={styles.mainContainer} source={images.MAIN_BG}>
      <Header
        onPressMethod={onPressLogout}
        onPressText={strings.common.HEADER_LOGOUT_BTN_TEXT}
      />
      <ModalPrompt
        isPromptActive={isPrompt}
        onAcceptMethod={deleteProfilePictureBtn}
        onDeclineMthod={() => setIsPrompt(false)}
        text={strings.common.PICTURE_DELETE_PROMPT_TEXT}
      />
      <ModalComment
        text={logState.logText}
        isComment={false}
        isModalActive={logState.showLog}
        onPressMethod={() => {
          setLogState({
            logText: [],
            showLog: false,
          });
        }}
      />
      <Toast config={toastConfig} ref={ref => Toast.setRef(ref)} />
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={isRefreshing}
            onRefresh={refreshComments}
          />
        }
        style={styles.mainScreenScroll}>
        <View style={styles.commentsIconContainer}>
          <TouchableOpacity
            testID={strings.testIds.MAIN_SCREEN_COMMENTS_ICON}
            onPress={goToCommentsScreen}
            style={styles.tooltipBtn}>
            <DefaultSvgIcon
              paths={svgPaths.commentsIcon}
              iconH={dimensions.MAIN_SCREEN_COMMENTS_ICON_WH}
              iconW={dimensions.MAIN_SCREEN_COMMENTS_ICON_WH}
              styles={styles.tooltipIcon}
              bgColor={colors[`${theme}`].AUTH_FORM_BG}
            />
            <Text
              style={[
                styles.tooltipText,
                {color: colors[`${theme}`].APP_COLOR},
              ]}>
              {userComments.length}
            </Text>
          </TouchableOpacity>
          <Text
            style={[
              styles.commentsText,
              {color: colors[`${theme}`].APP_COLOR},
            ]}>
            {strings.common.MAIN_SCREEN_COMMENTS_ICON_TEXT}
          </Text>
        </View>
        <Animated.View
          style={[styles.leadDescriptionContainer, {opacity: opacityValue}]}>
          <View
            style={[
              styles.descriptionBox,
              {backgroundColor: colors[`${theme}`].AUTH_FORM_BG},
            ]}>
            <View style={styles.topContainer}>
              <View style={styles.logContainer}>
                {userLog.length > 0
                  ? userLog[0].log.map((item, index) => {
                      if (index < dimensions.MAX_SHOWN_LOG_COUNT) {
                        return (
                          <Text
                            key={index}
                            style={[
                              styles.logText,
                              {color: colors[`${theme}`].APP_COLOR},
                            ]}>
                            {index + 1}.{item.date}
                          </Text>
                        );
                      }
                    })
                  : null}
                <TouchableOpacity
                  onPress={() =>
                    setLogState({
                      logText: userLog[0].log,
                      showLog: true,
                    })
                  }
                  style={[
                    styles.logShowBtn,
                    {backgroundColor: colors[`${theme}`].AUTH_FORM_BTN_BG},
                  ]}>
                  <Text
                    style={[
                      styles.logText,
                      {color: colors[`${theme}`].APP_COLOR},
                    ]}>
                    {strings.common.MORE_LOGS_BTN_TEXT}
                  </Text>
                </TouchableOpacity>
              </View>
              <Animated.View
                style={[
                  styles.anonymousIconContainer,
                  {
                    backgroundColor: colors[`${theme}`].AUTH_FORM_BTN_BG,
                    transform: [{translateX: transformValue}],
                  },
                ]}>
                {profilePicture.length > 0 ? (
                  <>
                    <TouchableOpacity
                      style={styles.pictureDeleteBtn}
                      onPress={() => setIsPrompt(true)}>
                      <SvgErrorImg
                        iconH={30}
                        iconW={30}
                        paths={svgPaths.errorIcon}
                        bgColor={'white'}
                      />
                    </TouchableOpacity>
                    <Image
                      style={{
                        width: dimensions.MAIN_SCREEN_USER_PICTURE_W,
                        height: dimensions.MAIN_SCREEN_USER_PICTURE_H,
                      }}
                      source={{
                        uri:
                          profilePicture.length > 0
                            ? `file://${profilePicture[0].image}`
                            : '../../src/img/mainBG.jpg',
                      }}
                    />
                  </>
                ) : (
                  <AnonymousUserIcon
                    paths={svgPaths.anonymousUser}
                    iconH={dimensions.MAIN_SCREEN_USER_PICTURE_H}
                    iconW={dimensions.MAIN_SCREEN_USER_PICTURE_W}
                  />
                )}
                <View style={styles.selectPictureBtn}>
                  <TouchableOpacity onPress={selectPictureFromGalleryBtn}>
                    <DefaultSvgIcon
                      iconH={dimensions.MAIN_SCREEN_GALLERY_AND_CAMERA_ICONS_WH}
                      iconW={dimensions.MAIN_SCREEN_GALLERY_AND_CAMERA_ICONS_WH}
                      paths={svgPaths.galleryIcon}
                      bgColor={'white'}
                      styles={{
                        marginRight:
                          dimensions.MAIN_SCREEN_GALLERY_AND_CAMERA_ICONS_MARGIN_R,
                      }}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity onPress={selectPictureFromCamera}>
                    <CameraIconSvg
                      iconW={dimensions.MAIN_SCREEN_GALLERY_AND_CAMERA_ICONS_WH}
                      iconH={dimensions.MAIN_SCREEN_GALLERY_AND_CAMERA_ICONS_WH}
                      paths={svgPaths.cameraIcon}
                    />
                  </TouchableOpacity>
                </View>
              </Animated.View>
            </View>
            {fieldNames.map((item, index) => {
              return (
                <CustomRow
                  fieldName={item}
                  key={index}
                  fieldValue={userInfo[serverResponse[index]]}
                />
              );
            })}
          </View>
        </Animated.View>
      </ScrollView>
    </ImageBackground>
  );
};

export default MainScreen;

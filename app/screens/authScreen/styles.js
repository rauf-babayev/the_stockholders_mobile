import {StyleSheet} from 'react-native';
import {dimensions} from '../../services/utils/constants';

export const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: dimensions.MAIN_SCREEN_PADDING_TOP,
  },
  container: {
    flex: 1,
  },
  scroll: {
    width: dimensions.AUTH_SCREEN_SCROLL_WH,
    height: dimensions.AUTH_SCREEN_SCROLL_WH,
    marginTop: dimensions.HEADER_CONTAINER_H,
  },
  scrollContent: {
    justifyContent: 'center',
    alignItems: 'center',
    flexGrow: 1,
  },
});

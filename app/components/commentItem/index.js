import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {styles} from './styles';
import {useSelector} from 'react-redux';
import {colors} from './../../services/utils/colors';
import {dimensions} from '../../services/utils/constants';
import {AnonymousUserIcon} from '../../src/img/svgImg';
import {svgPaths} from './../../services/utils/svgPaths';
import {strings} from '../../services/utils/strings';

const CommentItem = ({onPressMethod, text}) => {
  const theme = useSelector(state => state.appReducer.appTheme);

  return (
    <TouchableOpacity
      style={[
        styles.mainContainer,
        {
          backgroundColor:
            colors[
              `${
                theme == strings.common.MAIN_APP_LIGHT_THEME
                  ? strings.common.APP_DARK_THEME_TEXT
                  : strings.common.APP_LIGHT_THEME_TEXT
              }`
            ].COMMENT_ITEM_BG,
        },
      ]}
      onPress={() => onPressMethod(text)}>
      <View
        style={[
          styles.anonymousIconContainer,
          {backgroundColor: colors[`${theme}`].AUTH_FORM_BTN_BG},
        ]}>
        <AnonymousUserIcon
          paths={svgPaths.anonymousUser}
          iconH={dimensions.COMMENTS_SCREEN_USER_PICTURE_H}
          iconW={dimensions.COMMENTS_SCREEN_USER_PICTURE_W}
        />
      </View>
      <Text
        style={[
          styles.commentText,
          {color: colors[`${theme}`].COMENT_ITEM_TEXT},
        ]}>
        {text.length < dimensions.COMMENT_MAX_VISIBLE_LENGTH
          ? text
          : `${text.slice(0, dimensions.COMMENT_MAX_VISIBLE_LENGTH)}...`}
      </Text>
    </TouchableOpacity>
  );
};

export default CommentItem;

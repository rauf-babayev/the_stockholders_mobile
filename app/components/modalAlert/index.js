import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {styles} from './styles';
import Modal from 'react-native-modal';
import {useSelector, useDispatch} from 'react-redux';
import {colors} from './../../services/utils/colors';
import {strings} from '../../services/utils/strings';
import {dimensions} from '../../services/utils/constants';
import {deactivateAlertAction} from '../../modules/redux/actions';

const ModalAlert = ({isModalActive}) => {
  const dispatch = useDispatch();
  const theme = useSelector(state => state.appReducer.appTheme);
  const alertText = useSelector(state => state.appReducer.alertText);
  const onPressMethod = () => {
    dispatch(deactivateAlertAction());
  };

  return (
    <Modal
      style={styles.modalWindow}
      useNativeDriver={true}
      backdropOpacity={dimensions.MODAL_WINDOW_BACKDROP_OPACITY}
      isVisible={isModalActive}
      backdropColor={colors[`${theme}`].MODAL_BACKDROP}
      onBackdropPress={onPressMethod}
      animationIn={strings.common.MODAL_WINDOW_ANIMATION_FADE_IN}
      animationOut={strings.common.MODAL_WINDOW_ANIMATION_FADE_OUT}>
      <View
        style={[
          styles.mainContainer,
          {backgroundColor: colors[`${theme}`].MODAL_BG},
        ]}>
        <Text style={[styles.alertText, {color: colors[`${theme}`].APP_COLOR}]}>
          {alertText}
        </Text>
        <TouchableOpacity
          testID={strings.testIds.COMMENT_MODAL_WINDOW_BTN}
          style={[
            styles.btnContainer,
            {backgroundColor: colors[`${theme}`].AUTH_FORM_BTN_BG},
          ]}
          onPress={onPressMethod}>
          <Text style={[styles.btnText, {color: colors[`${theme}`].APP_COLOR}]}>
            {strings.common.MODAL_WINDOW_CLOSE_TEXT}
          </Text>
        </TouchableOpacity>
      </View>
    </Modal>
  );
};

export default ModalAlert;

import {combineReducers} from 'redux';

const {formReducer} = require('./reducers/formReducer');
const {appReducer} = require('./reducers/appReducer');
const {profileReducer} = require('./reducers/profileReducer');

const rootReducer = combineReducers({
  formReducer: formReducer,
  appReducer: appReducer,
  profileReducer: profileReducer,
});

export default rootReducer;

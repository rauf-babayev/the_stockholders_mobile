import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import MainScreen from './../../../screens/mainScreen/index';
import CommentsScreen from '../../../screens/commentsScreen';
import {strings} from '../../../services/utils/strings';

const RootStack = createStackNavigator();

const MainStackNav = () => {
  return (
    <RootStack.Navigator
      screenOptions={{headerShown: false, animationEnabled: false}}>
      <RootStack.Screen
        name={strings.routes.MAIN_STACK_MAIN_SCREEN_ROUTE}
        component={MainScreen}
      />
      <RootStack.Screen
        name={strings.routes.MAIN_STACK_COMMENTS_SCREEN_ROUTE}
        component={CommentsScreen}
      />
    </RootStack.Navigator>
  );
};

export default MainStackNav;

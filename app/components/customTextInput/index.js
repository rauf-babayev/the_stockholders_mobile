import React, {useState, useRef, useEffect} from 'react';
import {
  TextInput,
  View,
  Text,
  Animated,
  TouchableWithoutFeedback,
} from 'react-native';
import {styles} from './styles';
import {strings} from '../../services/utils/strings';
import {useSelector} from 'react-redux';
import {colors} from './../../services/utils/colors';
import {dimensions} from '../../services/utils/constants';

const CustomTextInput = ({
  formState,
  setFormState,
  placeholder,
  label,
  isSecure,
  testID,
}) => {
  const [animState, setAnimState] = useState({
    from: dimensions.CUSTOM_TEXT_INPUT_ANIM_VALUE_BOTTOM,
    to: dimensions.CUSTOM_TEXT_INPUT_ANIM_VALUE_TOP,
  });
  const theme = useSelector(state => state.appReducer.appTheme);
  const textINputRef = useRef();
  const initialAnimValue = useRef(new Animated.Value(animState.from)).current;

  const animStart = () => {
    Animated.timing(initialAnimValue, {
      toValue: animState.to,
      duration: dimensions.CUSTOM_TEXT_INUT_ANIMATION_DURATION,
      useNativeDriver: true,
    }).start();
  };

  useEffect(() => {
    if (!formState[`${placeholder.toLowerCase()}`]) {
      textINputRef.current.blur();
    }
  }, []);

  return (
    <View style={styles.mainContainer}>
      <Animated.View
        style={[
          styles.textAnimContainer,
          {
            transform: [{translateY: initialAnimValue}],
          },
        ]}>
        <TouchableWithoutFeedback
          onPress={() => {
            textINputRef.current.focus();
          }}>
          <Text style={[styles.text, {color: colors[`${theme}`].APP_COLOR}]}>
            {label}
          </Text>
        </TouchableWithoutFeedback>
      </Animated.View>
      <View>
        <TextInput
          ref={textINputRef}
          testID={testID}
          value={formState[`${placeholder.toLowerCase()}`]}
          style={[
            styles.formInput,
            {
              borderBottomColor: colors[`${theme}`].APP_COLOR,
              color: colors[`${theme}`].APP_COLOR,
            },
          ]}
          secureTextEntry={isSecure}
          autoCapitalize={strings.common.CUSTOM_TEXT_INPUT_AUTO_CAPITALIZE_TYPE}
          onChangeText={text =>
            setFormState({...formState, [`${placeholder.toLowerCase()}`]: text})
          }
          onBlur={() => {
            if (!formState[`${placeholder.toLowerCase()}`]) {
              animStart();
              setAnimState({
                from: dimensions.CUSTOM_TEXT_INPUT_ANIM_VALUE_BOTTOM,
                to: dimensions.CUSTOM_TEXT_INPUT_ANIM_VALUE_TOP,
              });
            }
          }}
          onFocus={() => {
            if (!formState[`${placeholder.toLowerCase()}`]) {
              animStart();
              setAnimState({
                from: dimensions.CUSTOM_TEXT_INPUT_ANIM_VALUE_TOP,
                to: dimensions.CUSTOM_TEXT_INPUT_ANIM_VALUE_BOTTOM,
              });
            }
          }}
        />
      </View>
    </View>
  );
};

export default CustomTextInput;

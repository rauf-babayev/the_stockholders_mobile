/* eslint-disable eqeqeq */
import React, {useState} from 'react';
import {View, TouchableOpacity} from 'react-native';
import {styles} from './styles';
import {useDispatch, useSelector} from 'react-redux';
import {changeThemeAction} from './../../modules/redux/actions/index';
import {colors} from './../../services/utils/colors';
import {strings} from '../../services/utils/strings';
import CustomSwitch from '../customSwitch';
import {BackIcon} from '../../src/img/svgImg';
import {svgPaths} from './../../services/utils/svgPaths';
import {LogoutSvgIcon} from './../../src/img/svgImg';
import ModalPrompt from '../modalPrompt';
import {dimensions} from '../../services/utils/constants';

const Header = ({onPressMethod, onPressText}) => {
  const dispatch = useDispatch();
  const [isPrompt, setIsPrompt] = useState(false);
  const theme = useSelector(state => state.appReducer.appTheme);
  const switchState = useSelector(state => state.appReducer.isDark);
  const changeTheme = () => {
    dispatch(
      changeThemeAction(
        switchState
          ? strings.common.APP_LIGHT_THEME_TEXT
          : strings.common.APP_DARK_THEME_TEXT,
        !switchState,
      ),
    );
  };

  return (
    <View
      style={[
        styles.mainContainer,
        {backgroundColor: colors[`${theme}`].AUTH_FORM_BG},
      ]}>
      <View style={styles.logoutBtnContainer}>
        <ModalPrompt
          isPromptActive={isPrompt}
          onAcceptMethod={onPressMethod}
          onDeclineMthod={() => setIsPrompt(false)}
          text={strings.common.HEADER_LOGOUT_MODAL_TEXT}
        />
        {onPressText ? (
          <TouchableOpacity
            style={styles.logoutBtn}
            testID={strings.testIds.HEADER_CLOSE_LOGOUT_BTN}
            onPress={() =>
              onPressText == strings.common.HEADER_LOGOUT_BACK_TEXT
                ? onPressMethod()
                : setIsPrompt(true)
            }>
            {onPressText == strings.common.HEADER_LOGOUT_BACK_TEXT ? (
              <BackIcon
                iconW={dimensions.MAIN_HEADER_BACK_ICON_WH}
                iconH={dimensions.MAIN_HEADER_BACK_ICON_WH}
                paths={svgPaths.backIcon}
                bgColor={colors[`${theme}`].AUTH_FORM_BTN_BG}
              />
            ) : (
              <LogoutSvgIcon
                iconW={dimensions.MAIN_HEADER_LOGOUT_ICON_WH}
                iconH={dimensions.MAIN_HEADER_LOGOUT_ICON_WH}
                paths={svgPaths.logoutIcon}
                bgColor={colors[`${theme}`].AUTH_FORM_BTN_BG}
              />
            )}
          </TouchableOpacity>
        ) : null}
      </View>
      <View style={styles.switchContainer}>
        <CustomSwitch
          isDark={switchState}
          changeTheme={changeTheme}
          theme={theme}
        />
      </View>
    </View>
  );
};

export default Header;

import {strings} from '../../../services/utils/strings';

export const authUserAction = userData => {
  return {
    type: strings.actionTypes.AUTH_USER,
    userData,
  };
};

export const navigateToScreenAction = navigation => {
  return {
    type: strings.actionTypes.NAVIGATE_TO_SCREEN,
    navigation,
  };
};

export const submitPassRecoveryAction = (username, setPassRecoveryState) => {
  return {
    type: strings.actionTypes.SUBMIT_PASS_RECOVERY,
    username,
    setPassRecoveryState,
  };
};

export const logoutUserAction = () => {
  return {
    type: strings.actionTypes.LOGOUT_USER,
  };
};

export const changeThemeAction = (theme, nextSwitchState) => {
  return {
    type: strings.actionTypes.CHANGE_THEME,
    theme,
    nextSwitchState,
  };
};

export const deactivateAlertAction = () => {
  return {
    type: strings.actionTypes.DEACTIVATE_ALERT,
  };
};

export const activateAlertAction = text => {
  return {
    type: strings.actionTypes.ACTIVATE_ALERT,
    text,
  };
};

export const setDeviceIpAction = ip => {
  return {
    type: strings.actionTypes.SET_DEVICE_IP,
    ip,
  };
};

export const setLoadingAction = isLoading => {
  return {
    type: strings.actionTypes.SET_LOADING,
    isLoading,
  };
};

export const selectPictureFromGalleryAction = () => {
  return {
    type: strings.actionTypes.SELECT_PICTURE_FROM_GALLERY,
  };
};

export const selectPictureFromCameraAction = () => {
  return {
    type: strings.actionTypes.SELECT_PICTURE_FROM_CAMERA,
  };
};

export const setProfilePictureAction = (path, login) => {
  return {
    type: strings.actionTypes.SET_PROFILE_PICTURE,
    path,
    login,
  };
};

export const deleteProfilePictureAction = login => {
  return {
    type: strings.actionTypes.DELETE_PROFILE_PICTURE,
    login,
  };
};

export const fetchUserCommentsAction = (login, pass) => {
  return {
    type: strings.actionTypes.FETCH_USER_COMMENTS,
    login,
    pass,
  };
};

export const setUserCommentsAction = data => {
  return {
    type: strings.actionTypes.SET_USER_COMMENTS,
    data,
  };
};

export const setNewCommentsAction = value => {
  return {
    type: strings.actionTypes.SET_NEW_COMMENTS,
    value,
  };
};

export const setNewUserLogAction = (login, ipAddress, date, platform) => {
  return {
    type: strings.actionTypes.SET_NEW_USER_LOG,
    login,
    ipAddress,
    date,
    platform,
  };
};

export const setOldUserLogAction = (login, ipAddress, date, platform) => {
  return {
    type: strings.actionTypes.SET_OLD_USER_LOG,
    login,
    ipAddress,
    date,
    platform,
  };
};

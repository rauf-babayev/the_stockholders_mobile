import {AppRegistry} from 'react-native';
import App from './app/App';
import {name as appName} from './app.json';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import {persistStore, persistReducer} from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import rootReducer from './app/modules/redux';
import createSagaMiddleware from 'redux-saga';
import React from 'react';
import {mainSagaWatcher} from './app/modules/redux/saga';
import {NavigationContainer} from '@react-navigation/native';
import {PersistGate} from 'redux-persist/integration/react';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);
const Saga = createSagaMiddleware();
let store = createStore(persistedReducer, applyMiddleware(Saga));
let persistor = persistStore(store);

const MainApp = () => {
  return (
    <NavigationContainer>
      <PersistGate loading={null} persistor={persistor}>
        <Provider store={store}>
          <App />
        </Provider>
      </PersistGate>
    </NavigationContainer>
  );
};

Saga.run(mainSagaWatcher);

AppRegistry.registerComponent(appName, () => MainApp);

import {StyleSheet} from 'react-native';
import {dimensions} from '../../services/utils/constants';

export const styles = StyleSheet.create({
  formContainer: {
    width: dimensions.AUTH_FORM_INPUT_W,
    paddingHorizontal: dimensions.AUTH_FORM_INPUT_PADDING_H,
    paddingVertical: dimensions.AUTH_FORM_INPUT_PADDING_V,
  },
  formText: {
    textAlign: 'center',
    fontSize: dimensions.APP_MAIN_FONT_SIZE,
  },
  btnContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: dimensions.AUTH_BTN_PADDING_V,
    paddingHorizontal: dimensions.AUTH_BTN_PADDING_H,
    marginTop: dimensions.AUTH_BTN_MARGIN_B,
  },
  btnText: {
    fontSize: dimensions.APP_MAIN_FONT_SIZE + 6,
  },
  toast: {
    paddingHorizontal: 15,
    paddingVertical: 5,
    borderRadius: 5,
    borderLeftWidth: 10,
    width: '90%',
  },
});

import React, {useState, useEffect, useRef} from 'react';
import {ScrollView, ImageBackground, Text, RefreshControl} from 'react-native';
import {styles} from './styles';
import CommentItem from '../../components/commentItem';
import {images} from './../../src/img/index';
import Header from './../../components/header/index';
import {useNavigation, CommonActions} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import {
  navigateToScreenAction,
  fetchUserCommentsAction,
} from '../../modules/redux/actions';
import {strings} from '../../services/utils/strings';
import ModalComment from '../../components/modalComment';
import {colors} from './../../services/utils/colors';
import Animated, {Easing} from 'react-native-reanimated';
import {dimensions} from '../../services/utils/constants';

const CommentsScreen = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [idTime, setIdTime] = useState('');
  const theme = useSelector(state => state.appReducer.appTheme);
  const userInfo = useSelector(state => state.formReducer.userInfo);
  const isRefreshing = useSelector(state => state.appReducer.isRefreshing);
  const userComments = useSelector(state => state.formReducer.userComments);
  const [commentState, setCommentState] = useState({
    showComment: false,
    commentText: '',
  });

  const onPressMethod = () => {
    clearInterval(idTime);
    dispatch(
      navigateToScreenAction(() => {
        navigation.dispatch(
          CommonActions.reset({
            routes: [{name: strings.routes.MAIN_STACK_MAIN_SCREEN_ROUTE}],
          }),
        );
      }),
    );
  };

  const onPressComment = comment => {
    setCommentState({
      showComment: true,
      commentText: comment,
    });
  };

  const onCloseComment = () => {
    setCommentState({
      showComment: false,
      commentText: '',
    });
  };

  const refreshComments = () => {
    dispatch(
      fetchUserCommentsAction(
        userInfo.account.login,
        userInfo.account.password,
      ),
    );
  };

  useEffect(() => {
    Animated.timing(opacityValue, {
      toValue: 1,
      duration: 500,
      useNativeDriver: true,
      easing: Easing.linear,
    }).start();

    const identificator = setInterval(() => {
      dispatch(
        fetchUserCommentsAction(
          userInfo.account.login,
          userInfo.account.password,
        ),
      );
    }, dimensions.COMMENTS_FETCH_DELAY);
    setIdTime(identificator);
  }, []);
  const opacityValue = useRef(new Animated.Value(0)).current;

  return (
    <ImageBackground style={styles.mainContainer} source={images.MAIN_BG}>
      <Header
        onPressText={strings.common.HEADER_CLOSE_BTN_TEXT}
        onPressMethod={onPressMethod}
      />
      <Animated.View
        style={[
          styles.commentsContainer,
          {
            backgroundColor: colors[`${theme}`].AUTH_FORM_BG,
            opacity: opacityValue,
          },
        ]}>
        <ScrollView
          testID={strings.testIds.COMMENTS_SCREEN_SCROLL}
          contentContainerStyle={styles.commentsScroll}
          refreshControl={
            <RefreshControl
              refreshing={isRefreshing}
              onRefresh={refreshComments}
            />
          }>
          <ModalComment
            text={commentState.commentText}
            isModalActive={commentState.showComment}
            onPressMethod={onCloseComment}
            isComment={true}
          />
          {userComments.length > 0 ? (
            userComments.map((item, index) => {
              return (
                <CommentItem
                  text={item}
                  onPressMethod={onPressComment}
                  key={index}
                />
              );
            })
          ) : (
            <Text
              style={[
                styles.noComment,
                {
                  color: colors[`${theme}`].APP_COLOR,
                },
              ]}>
              {strings.common.COMMENTS_SCREEN_NO_COMMENTS_TEXT}
            </Text>
          )}
        </ScrollView>
      </Animated.View>
    </ImageBackground>
  );
};

export default CommentsScreen;

import React, {useRef, useEffect} from 'react';
import {Animated, ImageBackground} from 'react-native';
import {styles} from './styles';
import {SvgImg} from '../../src/img/svgImg';
import {svgPaths} from '../../services/utils/svgPaths';
import {dimensions} from '../../services/utils/constants';
import {images} from '../../src/img';
import {Easing} from 'react-native-reanimated';

const SplashScreen = () => {
  useEffect(() => {
    Animated.sequence([
      Animated.timing(logoOpacityValue, {
        toValue: 0.8,
        duration: 1000,
        useNativeDriver: true,
        easing: Easing.linear,
      }),
      Animated.timing(logoOpacityValue, {
        toValue: 0.5,
        duration: 500,
        useNativeDriver: true,
        easing: Easing.linear,
      }),
    ]).start();

    Animated.timing(imageOpacityValue, {
      toValue: 0.5,
      duration: 500,
      useNativeDriver: true,
      delay: 1500,
      easing: Easing.linear,
    }).start();
  }, []);
  const logoOpacityValue = useRef(new Animated.Value(0)).current;
  const imageOpacityValue = useRef(new Animated.Value(0)).current;

  return (
    <ImageBackground source={images.MAIN_BG} style={styles.mainContainer}>
      <Animated.View style={{opacity: logoOpacityValue}}>
        <SvgImg
          paths={svgPaths.mainLogo}
          iconH={dimensions.SPLASH_SCREEN_SVG_LOGO_H}
          iconW={dimensions.SPLASH_SCREEN_SVG_LOGO_W}
        />
      </Animated.View>
      <Animated.Image
        style={[styles.mainImage, {opacity: imageOpacityValue}]}
        source={images.SPLASH_IMG}
      />
    </ImageBackground>
  );
};

export default SplashScreen;

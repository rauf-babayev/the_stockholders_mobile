/* eslint-disable no-alert */
/* eslint-disable no-fallthrough */
import React, {useState, useEffect, useRef} from 'react';
import {TouchableOpacity, Text, Keyboard, Platform} from 'react-native';
import {styles} from './styles';
import CustomTextInput from '../customTextInput';
import {useDispatch, useSelector} from 'react-redux';
import {
  authUserAction,
  navigateToScreenAction,
} from '../../modules/redux/actions';
import {getFullDate} from '../../modules/functions';
import {strings} from '../../services/utils/strings';
import {useNavigation, CommonActions} from '@react-navigation/native';
import {colors} from './../../services/utils/colors';
import Animated from 'react-native-reanimated';
import {mainComponentAnimation} from '../animations';

const AuthForm = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const theme = useSelector(state => state.appReducer.appTheme);
  const deviceIp = useSelector(state => state.appReducer.deviceIp);
  const [formState, setFormState] = useState({
    login: '',
    password: '',
  });
  const formInitValue = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    mainComponentAnimation(formInitValue, 1, 1000);
    setFormState({
      login: '',
      password: '',
    });
  }, []);

  const authUserBtn = () => {
    Keyboard.dismiss();
    dispatch(
      authUserAction({
        login: formState.login,
        password: formState.password,
        ipAddress: deviceIp,
        date: getFullDate(new Date()),
        platform: Platform.OS,
      }),
    );
  };

  const passRecoveryBtn = () => {
    dispatch(
      navigateToScreenAction(() => {
        navigation.dispatch(
          CommonActions.reset({
            routes: [
              {name: strings.routes.AUTH_STACK_PASS_RECOVERY_ROUTE_NAME},
            ],
          }),
        );
      }),
    );
  };

  return (
    <Animated.View
      testID={strings.testIds.AUTH_FORM_MAIN_CONTAINER}
      style={[
        styles.formContainer,
        {
          backgroundColor: colors[`${theme}`].AUTH_FORM_BG,
          opacity: formInitValue,
        },
      ]}>
      <CustomTextInput
        testID={strings.testIds.AUTH_FORM_USERNAME_INPUT}
        placeholder={strings.common.AUTH_FORM_USERNAME_PLACEHOLDER}
        formState={formState}
        setFormState={setFormState}
        label={strings.common.AUTH_FORM_USERNAME_INPUT_LABEL}
        isSecure={false}
      />
      <CustomTextInput
        testID={strings.testIds.AUTH_FORM_PASSWORD_INPUT_LABEL}
        placeholder={strings.common.AUTH_FORM_PASSWORD_PLACEHOLDER}
        formState={formState}
        setFormState={setFormState}
        label={strings.common.AUTH_FORM_PASSWORD_INPUT_LABEL}
        isSecure={true}
      />
      <TouchableOpacity
        testID={strings.testIds.AUTH_FORM_MAIN_BTN}
        onPress={authUserBtn}
        style={[
          styles.btnContainer,
          {backgroundColor: colors[`${theme}`].AUTH_FORM_BTN_BG},
        ]}>
        <Text style={[styles.btnText, {color: colors[`${theme}`].APP_COLOR}]}>
          {strings.common.AUTH_FORM_SIGNIN_BTN_TEXT}
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        testID={strings.testIds.AUTH_FORM_PASS_RECOVERY_BTN}
        style={styles.forgotPassBtnContainer}
        onPress={passRecoveryBtn}>
        <Text
          style={[
            styles.forgotPassBtnText,
            {color: colors[`${theme}`].APP_COLOR},
          ]}>
          {strings.common.AUTH_FORM_PASS_FORGET_BTN_TEXT}
        </Text>
      </TouchableOpacity>
    </Animated.View>
  );
};

export default AuthForm;

import {Dimensions} from 'react-native';
import {dimensionSetter} from '../../modules/functions';

const screenWidth = Dimensions.get('window').width;

export const dimensions = {
  SPLASH_SCREEN_LOGO_ANIMATION_DURATION: 2000,
  AUTH_FORM_INPUT_W: dimensionSetter(screenWidth, 300, 100, 300),
  AUTH_FORM_INPUT_PADDING_H: dimensionSetter(screenWidth, 25, 5, 15),
  AUTH_FORM_INPUT_PADDING_V: dimensionSetter(screenWidth, 40, 20, 20),
  AUTH_FORM_CONTAINER_MAX_HEIGHT: dimensionSetter(screenWidth, 550, 50, 50),
  // AUTH_FORM_BORDER_W: 5,
  APP_MAIN_FONT_SIZE: dimensionSetter(screenWidth, 22, 6, 14),
  AUTH_INPUT_MARGIN_B: dimensionSetter(screenWidth, 30, 10, 20),
  AUTH_BTN_PADDING_V: dimensionSetter(screenWidth, 5, 2, 5),
  // AUTH_BTN_PADDING_H: 20,
  // AUTH_BTN_BORDER_W: 3,
  // AUTH_BTN_BORDER_RADIUS: 20,
  AUTH_BTN_MARGIN_B: dimensionSetter(screenWidth, 20, 10, 10),
  TEXT_INPUT_ERROR_ICON_WH: dimensionSetter(screenWidth, 20, 5, 10),
  FORM_TEXT_INPUT_W: '100%',
  FORM_TEXT_INPUT_HEIGHT: dimensionSetter(screenWidth, 50, 10, 20),
  FORM_TEXT_INPUT_MARGIN_B: dimensionSetter(screenWidth, 10, 5, 10),
  FORM_TEXT_INPUT_BORDER_W: dimensionSetter(screenWidth, 2, 1, 0),
  // FORM_TEXT_INPUT_BORDER_RADIUS: 20,
  FORM_TEXT_INPUT_PADDING_H: dimensionSetter(screenWidth, 10, 2, 4),
  FORM_TEXT_INPUT_ERROR_ICON_POSITION_MARGIN_TOP: dimensionSetter(
    screenWidth,
    55,
    5,
    5,
  ),
  FORM_TEXT_INPUT_ERROR_ICON_POSITION_MARGIN_RIGHT: 15,
  SPLASH_SCREEN_SVG_LOGO_W: dimensionSetter(screenWidth, 300, 100, 200),
  SPLASH_SCREEN_SVG_LOGO_H: dimensionSetter(screenWidth, 100, 50, 100),
  PASS_RECOVERY_TEXT_FONT_WEIGHT: '700',
  COMMENT_MAX_VISIBLE_LENGTH: dimensionSetter(screenWidth, 65, 10, 45),
  COMMENT_CONTAINER_W: '100%',
  COMMENT_CONTAINER_H: dimensionSetter(screenWidth, 100, 30, 60),
  COMMENT_ITEM_MARGIN_BOTTOM: dimensionSetter(screenWidth, 20, 10, 5),
  COMMENT_ITEM_PADDING: dimensionSetter(screenWidth, 10, 5, 5),
  LEAD_DESCRIPTION_ROW_MARGIN_H: dimensionSetter(screenWidth, 20, 5, 10),
  LEAD_DESCRIPTION_ROW_FIELD_NAME_W: '30%',
  LEAD_DESCRIPTION_ROW_FIELD_VALUE_W: '70%',
  LEAD_DESCRIPTION_ROW_MARGIN_NAME_VALUE: dimensionSetter(
    screenWidth,
    10,
    2,
    4,
  ),
  LEAD_DESCRIPTION_ROW_MIN_HEIGHT: dimensionSetter(screenWidth, 50, 10, 20),
  LEAD_DESCRIPTION_ROW_INPUT_HEIGHT: dimensionSetter(screenWidth, 30, 10, 20),
  CUSTOM_TEXT_INPUT_MARGIN_BOTTOM: dimensionSetter(screenWidth, 10, 2, 14),
  HEADER_CONTAINER_W: '100%',
  HEADER_CONTAINER_H: dimensionSetter(screenWidth, 50, 10, 30),
  HEADER_CONTAINER_PADDING_H: dimensionSetter(screenWidth, 20, 10, 10),
  MODAL_WINDOW_BACKDROP_OPACITY: 0.7,
  MODAL_WINDOW_WIDTH: '80%',
  MODAL_WINDOW_PADDING: dimensionSetter(screenWidth, 10, 5, 10),
  MODAL_WINDOW_COMMENT_WIDTH: '100%',
  MODAL_WINDOW_COMMENT_MARGIN_BOTTOM: dimensionSetter(screenWidth, 15, 5, 5),
  MODAL_WINDOW_COMMENT_PADDING: dimensionSetter(screenWidth, 10, 4, 4),
  MODAL_WINDOW_MIN_HEIGHT: dimensionSetter(screenWidth, 150, 50, 100),
  MODAL_WINDOW_COMMENT_TEXT_LINE_HEIGHT: dimensionSetter(
    screenWidth,
    35,
    10,
    5,
  ),
  // MODAL_WINDOW_BTN_PADDING_H: 50,
  // MODAL_WINDOW_BTN_PADDING_V: 10,
  // MODAL_WINDOW_BTN_BORDER_RADIUS: 100,
  COMMENTS_SCREEN_PADDING_TOP: dimensionSetter(screenWidth, 50, 10, 30),
  COMMENTS_SCREEN_CONTAINER_WH: '80%',
  // COMMENTS_SCREEN_CONTAINER_MARGIN_V: 30,
  COMMENTS_SCREEN_SCROLL_PADDINGS: dimensionSetter(screenWidth, 20, 10, 5),
  MAIN_SCREEN_COMMENTS_ICON_WH: dimensionSetter(screenWidth, 70, 20, 30),
  MAIN_SCREEN_USER_PICTURE_W: dimensionSetter(screenWidth, 140, 50, 100),
  MAIN_SCREEN_USER_PICTURE_H: dimensionSetter(screenWidth, 170, 50, 100),
  MAIN_SCREEN_PADDING_TOP: dimensionSetter(screenWidth, 50, 10, 30),
  MAIN_SCREEN_COMMENTS_CONTAINER_PADDING_H: dimensionSetter(
    screenWidth,
    30,
    10,
    10,
  ),
  MAIN_SCREEN_DESCRIPTION_CONTAINER_PADDING: dimensionSetter(
    screenWidth,
    30,
    10,
    10,
  ),
  MAIN_SCREEN_DESCRIPTION_PADDING_BTM: dimensionSetter(screenWidth, 10, 5, 10),
  MAIN_SCREEN_DESCRIPTION_HEIGHT: '100%',
  MAIN_SCREEN_TOOLTIP_ICON_MARGIN_BTM: dimensionSetter(screenWidth, 15, 0, 0),
  MAIN_SCREEN_USER_PICTURE_MARGIN_BTM: dimensionSetter(screenWidth, 10, 5, 10),
  MAIN_SCREEN_USER_PICTURE_PADDING: dimensionSetter(screenWidth, 10, 6, 15),
  MAIN_SCREEN_USER_PICTURE_MARGIN_OUTSIDE_RIGHT: dimensionSetter(
    screenWidth,
    -10,
    -5,
    -5,
  ),
  MAIN_SCREEN_USER_PICTURE_MARGIN_OUTSIDE_LEFT: dimensionSetter(
    screenWidth,
    -50,
    -5,
    -5,
  ),
  CUSTOM_SWITCH_BG_W: dimensionSetter(screenWidth, 40, 15, 30),
  CUSTOM_SWITCH_BG_H: dimensionSetter(screenWidth, 15, 5, 10),
  CUSTOM_SWITCH_ICON_WH: dimensionSetter(screenWidth, 20, 5, 20),
  CUSTOM_SWITCH_MOON_ICON_WH: dimensionSetter(screenWidth, 15, 5, 15),
  CUSTOM_SWITCH_SUN_ICON_WH: dimensionSetter(screenWidth, 15, 5, 15),
  CUSTOM_SWITCH_BORDER_RADIUS: 50,
  CUSTOM_SWITCH_ICON_MARGIN_TOP: dimensionSetter(screenWidth, -3, -0.5, -4),
  HEADER_LOGOUT_BTN_PADDING_H: 10,
  AUTH_FORM_INPUT_MARGIN_TOP: dimensionSetter(screenWidth, 15, 5, 10),
  AUTH_FORM_INPUT_VALUE_FROM: 10,
  AUTH_FORM_INPUT_VALUE_TO: dimensionSetter(screenWidth, -35, -5, -20),
  AUTH_FORM_INPUT_PADDING_TOP: dimensionSetter(screenWidth, 40, 5, 10),
  COMMENTS_SCREEN_USER_PICTURE_W: dimensionSetter(screenWidth, 30, 10, 40),
  COMMENTS_SCREEN_USER_PICTURE_H: dimensionSetter(screenWidth, 40, 10, 50),
  MAIN_HEADER_BACK_ICON_WH: dimensionSetter(screenWidth, 25, 5, 15),
  LOADER_SCALE: dimensionSetter(screenWidth, 3, 1.5, 0),
  MAIN_HEADER_LOGOUT_ICON_WH: dimensionSetter(screenWidth, 30, 10, 15),
  MAIN_SCREEN_GALLERY_AND_CAMERA_ICONS_WH: dimensionSetter(
    screenWidth,
    30,
    5,
    25,
  ),
  MAIN_SCREEN_GALLERY_AND_CAMERA_ICONS_MARGIN_R: dimensionSetter(
    screenWidth,
    50,
    30,
    40,
  ),
  COMMENT_ITEM_ICON_CONTAINER_HEIGHT: '100%',
  COMMENT_ITEM_ICON_MARGIN_R: 10,
  LOADER_INDICATOR_WH: '100%',
  CUSTOM_ROW_MAX_LENGTH_LIMIT: 20,
  CUSTOM_TEXT_INUT_ANIMATION_DURATION: 300,
  CUSTOM_TEXT_INPUT_ANIM_VALUE_TOP: -40,
  CUSTOM_TEXT_INPUT_ANIM_VALUE_BOTTOM: 0,
  CUSTOM_TEXT_INPUT_CONTAINER_MARGINS: 10,
  MODAL_ALERT_BTN_WIDTH: '100%',
  COMMENT_ITEM_CONTAINER_MAX_HEIGHT: '60%',
  COMMENT_TEXT_MARGIN_BOTTOM: 10,
  COMMENT_ITEM_SCROLL_WIDTH: '100%',
  MODAL_PROMPT_BTN_WIDTH: '45%',
  MODAL_PROMPT_BTN_WRAPPER_WIDTH: '100%',
  CUSTOM_TOAST_PADDING_H: 15,
  CUSTOM_TOAST_PADDING_V: 5,
  CUSTOM_TOAST_BORDER_R: 5,
  CUSTOM_TOAST_BORDER_W: 10,
  CUSTOM_TOAST_CONTAINER_WIDTH: '90%',
  AUTH_REQUEST_LOGIN_MIN_LENGTH: 3,
  AUTH_REQUEST_PASSWORD_MIN_LENGTH: 8,
  PASS_RECOVERY_TOAST_VISIBILITY_TIME: 4000,
  PASS_RECOVERY_TOAST_TOP_OFFSET: 60,
  PASS_RECOVERY_TOAST_BOTTOM_OFFSET: 100,
  AUTH_SCREEN_SCROLL_WH: '100%',
  COMMENTS_FETCH_DELAY: 10000,
  SPLASH_SCREEN_WH: '100%',
  LOG_TEXT_FONT_SIZE: dimensionSetter(screenWidth, 14, 4, 6),
  MAX_SHOWN_LOG_COUNT: 6,
};

import {dimensions} from '../../services/utils/constants';
import Animated, {Easing} from 'react-native-reanimated';

export const mainLogoAnimation = value =>
  Animated.timing(value, {
    toValue: 1,
    duration: dimensions.SPLASH_SCREEN_LOGO_ANIMATION_DURATION,
    useNativeDriver: true,
  }).start();

export const mainComponentAnimation = (initValue, toValue, duration) => {
  Animated.timing(initValue, {
    toValue: toValue,
    duration: duration,
    useNativeDriver: true,
    easing: Easing.linear,
  }).start();
};

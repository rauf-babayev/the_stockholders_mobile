import React, {useState, useEffect} from 'react';
import SplashScreen from './screens/splashScreen';
import {useSelector, useDispatch} from 'react-redux';
import AuthStackNav from './modules/navigation/authStackNav';
import MainStackNav from './modules/navigation/mainStackNav/index';
import ModalAlert from './components/modalAlert';
import NetInfo from '@react-native-community/netinfo';
import {
  setDeviceIpAction,
  activateAlertAction,
} from './modules/redux/actions/index';
import {strings} from './services/utils/strings';

const App = () => {
  const [firstScreen, setFirstScreen] = useState(<SplashScreen />);
  const [secondScreen, setSecondScreen] = useState(<SplashScreen />);
  const isUserAuthorized = useSelector(
    state => state.formReducer.isUserAuthorized,
  );
  const isAlert = useSelector(state => state.appReducer.isAlert);
  const dispatch = useDispatch();

  useEffect(() => {
    setTimeout(() => {
      setFirstScreen(<AuthStackNav />);
    }, 5000);
    setTimeout(() => {
      setSecondScreen(<MainStackNav />);
    }, 5000);

    const unsubscribe = NetInfo.addEventListener(state => {
      if (
        state.isInternetReachable &&
        state.type == strings.common.NET_INFO_TYPE_WIFI
      ) {
        dispatch(setDeviceIpAction(state.details.ipAddress));
      } else if (
        state.isInternetReachable &&
        state.type == strings.common.NET_INFO_TYPE_CELLULAR
      ) {
        dispatch(setDeviceIpAction(state.details.cellularGeneration));
      } else {
        dispatch(activateAlertAction(strings.error.CONNECTION_LOST_ERROR));
      }
    });

    return () => {
      unsubscribe();
    };
  }, []);

  return (
    <>
      <ModalAlert isModalActive={isAlert} />
      {!isUserAuthorized ? firstScreen : secondScreen}
    </>
  );
};

export default App;

import React from 'react';
import {View, Text, ScrollView} from 'react-native';
import {styles} from './styles';
import Modal from 'react-native-modal';
import {useSelector} from 'react-redux';
import {colors} from './../../services/utils/colors';
import {strings} from '../../services/utils/strings';
import {dimensions} from '../../services/utils/constants';

const ModalComment = ({text, onPressMethod, isModalActive, isComment}) => {
  const theme = useSelector(state => state.appReducer.appTheme);

  return (
    <Modal
      useNativeDriver={true}
      backdropOpacity={dimensions.MODAL_WINDOW_BACKDROP_OPACITY}
      isVisible={isModalActive}
      backdropColor={colors[`${theme}`].MODAL_BACKDROP}
      onBackdropPress={onPressMethod}
      animationIn={strings.common.MODAL_WINDOW_ANIMATION_FADE_IN}
      animationOut={strings.common.MODAL_WINDOW_ANIMATION_FADE_OUT}>
      <View
        style={[
          styles.mainContainer,
          {backgroundColor: colors[`${theme}`].MODAL_BG},
        ]}>
        <View
          style={[
            styles.commentContainer,
            {
              backgroundColor:
                colors[
                  `${
                    theme == strings.common.APP_LIGHT_THEME_TEXT
                      ? strings.common.APP_DARK_THEME_TEXT
                      : strings.common.APP_LIGHT_THEME_TEXT
                  }`
                ].COMMENT_ITEM_BG,
            },
          ]}>
          <ScrollView
            style={styles.scroll}
            contentContainerStyle={styles.scrollContent}>
            {isComment ? (
              <Text
                style={[
                  styles.commentText,
                  {color: colors[`${theme}`].COMENT_ITEM_TEXT},
                ]}>
                {text}
              </Text>
            ) : (
              text.map((item, index) => {
                return (
                  <Text
                    key={index}
                    style={[
                      styles.commentText,
                      {
                        color: colors[`${theme}`].COMENT_ITEM_TEXT,
                      },
                    ]}>
                    {index + 1}. {strings.common.LOG_DATE_TEXT} {item.date} /{' '}
                    {strings.common.LOG_OS_TEXT} {item.platform} /{' '}
                    {strings.common.LOG_IP_TEXT} {item.ipAddress}
                  </Text>
                );
              })
            )}
          </ScrollView>
        </View>
      </View>
    </Modal>
  );
};

export default ModalComment;

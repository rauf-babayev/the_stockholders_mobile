import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {styles} from './styles';
import Modal from 'react-native-modal';
import {useSelector} from 'react-redux';
import {colors} from './../../services/utils/colors';
import {strings} from '../../services/utils/strings';
import {dimensions} from '../../services/utils/constants';

const ModalPrompt = ({
  isPromptActive,
  onAcceptMethod,
  onDeclineMthod,
  text,
}) => {
  const theme = useSelector(state => state.appReducer.appTheme);

  return (
    <Modal
      style={styles.modalWindow}
      useNativeDriver={true}
      backdropOpacity={dimensions.MODAL_WINDOW_BACKDROP_OPACITY}
      isVisible={isPromptActive}
      backdropColor={colors[`${theme}`].MODAL_BACKDROP}
      onBackdropPress={onDeclineMthod}
      animationIn={strings.common.MODAL_WINDOW_ANIMATION_FADE_IN}
      animationOut={strings.common.MODAL_WINDOW_ANIMATION_FADE_OUT}>
      <View
        style={[
          styles.mainContainer,
          {backgroundColor: colors[`${theme}`].MODAL_BG},
        ]}>
        <Text style={[styles.alertText, {color: colors[`${theme}`].APP_COLOR}]}>
          {text}
        </Text>
        <View style={styles.btnWrapper}>
          <TouchableOpacity
            testID={strings.testIds.COMMENT_MODAL_WINDOW_BTN}
            style={[
              styles.btnContainer,
              {backgroundColor: colors[`${theme}`].AUTH_FORM_BTN_BG},
            ]}
            onPress={onDeclineMthod}>
            <Text
              style={[styles.btnText, {color: colors[`${theme}`].APP_COLOR}]}>
              {strings.common.MODAL_PROMPT_DECLINE_TEXT}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            testID={strings.testIds.COMMENT_MODAL_WINDOW_BTN}
            style={[
              styles.btnContainer,
              {backgroundColor: colors[`${theme}`].AUTH_FORM_BTN_BG},
            ]}
            onPress={onAcceptMethod}>
            <Text
              style={[styles.btnText, {color: colors[`${theme}`].APP_COLOR}]}>
              {strings.common.MODAL_PROMPT_ACCEPT_TEXT}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

export default ModalPrompt;

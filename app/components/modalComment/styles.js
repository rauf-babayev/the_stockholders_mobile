import {StyleSheet} from 'react-native';
import {dimensions} from './../../services/utils/constants';
import {colors} from '../../services/utils/colors';

export const styles = StyleSheet.create({
  mainContainer: {
    width: dimensions.MODAL_WINDOW_WIDTH,
    alignSelf: 'center',
    padding: dimensions.MODAL_WINDOW_PADDING,
    alignItems: 'center',
    maxHeight: dimensions.COMMENT_ITEM_CONTAINER_MAX_HEIGHT,
  },
  commentContainer: {
    width: dimensions.MODAL_WINDOW_COMMENT_WIDTH,
    backgroundColor: colors.common.MODAL_WINDOW_MAIN_BG,
    padding: dimensions.MODAL_WINDOW_COMMENT_PADDING,
    minHeight: dimensions.MODAL_WINDOW_MIN_HEIGHT,
  },
  commentText: {
    fontSize: dimensions.APP_MAIN_FONT_SIZE,
    textAlign: 'center',
    lineHeight: dimensions.MODAL_WINDOW_COMMENT_TEXT_LINE_HEIGHT,
    marginBottom: dimensions.COMMENT_TEXT_MARGIN_BOTTOM,
  },
  btnContainer: {
    backgroundColor: colors.common.MODAL_WINDOW_MAIN_BG,
    paddingHorizontal: dimensions.MODAL_WINDOW_BTN_PADDING_H,
    paddingVertical: dimensions.MODAL_WINDOW_BTN_PADDING_V,
    borderRadius: dimensions.MODAL_WINDOW_BTN_BORDER_RADIUS,
  },
  btnText: {
    fontSize: dimensions.APP_MAIN_FONT_SIZE,
  },
  scroll: {
    width: dimensions.COMMENT_ITEM_SCROLL_WIDTH,
  },
  scrollContent: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

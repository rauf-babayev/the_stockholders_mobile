export const fieldNames = [
  'Name',
  'Birth year',
  'Course',
  'Email',
  'Phone',
  'Status',
];

export const serverResponse = [
  'fullName',
  'date',
  'course',
  'email',
  'phone',
  'leadStatus',
];

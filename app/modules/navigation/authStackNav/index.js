import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import AuthScreen from './../../../screens/authScreen/index';
import PassRecoveryScreen from '../../../screens/passRecoveryScreen';
import {strings} from './../../../services/utils/strings';

const RootStack = createStackNavigator();

const AuthStackNav = () => {
  return (
    <RootStack.Navigator
      screenOptions={{headerShown: false, animationEnabled: false}}>
      <RootStack.Screen
        name={strings.routes.AUTH_STACK_LOGIN_ROUTE_NAME}
        component={AuthScreen}
      />
      <RootStack.Screen
        name={strings.routes.AUTH_STACK_PASS_RECOVERY_ROUTE_NAME}
        component={PassRecoveryScreen}
      />
    </RootStack.Navigator>
  );
};

export default AuthStackNav;

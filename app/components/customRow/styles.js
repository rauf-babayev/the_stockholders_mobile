import {StyleSheet} from 'react-native';
import {dimensions} from '../../services/utils/constants';

export const styles = StyleSheet.create({
  mainContainer: {
    minHeight: dimensions.LEAD_DESCRIPTION_ROW_MIN_HEIGHT,
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: dimensions.LEAD_DESCRIPTION_ROW_MARGIN_H,
  },
  fieldName: {
    fontSize: dimensions.APP_MAIN_FONT_SIZE - 2,
    textAlignVertical: 'bottom',
    marginRight: dimensions.LEAD_DESCRIPTION_ROW_MARGIN_NAME_VALUE,
    flex: 0.3,
  },
  fieldInput: {
    flex: 0.7,
    minHeight: dimensions.LEAD_DESCRIPTION_ROW_INPUT_HEIGHT,
    borderBottomWidth: 1,
    padding: 0,
  },
});

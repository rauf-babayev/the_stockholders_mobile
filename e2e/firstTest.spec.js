
describe('Example', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });
  const sleep = duration =>
    new Promise(resolve => setTimeout(() => resolve(), duration));
  it('Main screen repair button text', async () => {
    await sleep(7000);
    await expect(element(by.id('auth_form_btn'))).toBeVisible();
  });
  // it('Area image test', async () => {
  //   await sleep(10000);
  //   await element(by.id('burger')).tap();
  //   await element(by.id('drawerOptionArea')).tap();
  //   await expect(element(by.id('areaImage'))).toBeVisible();
  // });
  // it('Area text test', async () => {
  //   await sleep(10000);
  //   await element(by.id('burger')).tap();
  //   await element(by.id('drawerOptionArea')).tap();
  //   await expect(element(by.id('areaText'))).toHaveText(
  //     string.AREA_TITLE_TEXT.join('\n'),
  //   );
  // });
});

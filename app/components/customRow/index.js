import React from 'react';
import {Text, View, TextInput} from 'react-native';
import {styles} from './styles';
import {useSelector} from 'react-redux';
import {colors} from './../../services/utils/colors';
import {dimensions} from '../../services/utils/constants';

const CustomRow = ({fieldName, fieldValue = 'undefined'}) => {
  const theme = useSelector(state => state.appReducer.appTheme);

  return (
    <View style={styles.mainContainer}>
      <Text style={[styles.fieldName, {color: colors[`${theme}`].APP_COLOR}]}>
        {fieldName}
      </Text>
      <TextInput
        multiline={true}
        editable={false}
        style={[
          styles.fieldInput,
          {
            color: colors[`${theme}`].APP_COLOR,
            borderBottomColor: colors[`${theme}`].APP_COLOR,
            fontSize:
              fieldValue.length >= dimensions.CUSTOM_ROW_MAX_LENGTH_LIMIT
                ? dimensions.APP_MAIN_FONT_SIZE - 4
                : dimensions.APP_MAIN_FONT_SIZE - 2,
          },
        ]}
        value={fieldValue}
      />
    </View>
  );
};

export default CustomRow;

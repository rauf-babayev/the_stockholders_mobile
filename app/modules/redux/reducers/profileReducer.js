import {strings} from '../../../services/utils/strings';

let initialState = {
  profilePicture: '',
  accounts: [{login: 'samir', image: null, log: null}],
};

export const profileReducer = (state = initialState, action) => {
  switch (action.type) {
    case strings.actionTypes.SET_PROFILE_PICTURE:
      return {
        ...state,
        accounts: state.accounts.map(item => {
          if (item.login == action.login) {
            return {...item, image: action.path};
          } else {
            return item;
          }
        }),
      };
    case strings.actionTypes.DELETE_PROFILE_PICTURE:
      return {
        ...state,
        accounts: state.accounts.map(item => {
          if (item.login == action.login) {
            return {...item, image: null};
          } else {
            return item;
          }
        }),
      };
    case strings.actionTypes.SET_NEW_USER_LOG:
      return {
        ...state,
        accounts: [
          ...state.accounts,
          {
            login: action.login,
            image: null,
            log: [
              {
                ipAddress: action.ipAddress,
                platform: action.platform,
                date: action.date,
              },
            ],
          },
        ],
      };
    case strings.actionTypes.SET_OLD_USER_LOG:
      let log = {
        ipAddress: action.ipAddress,
        date: action.date,
        platform: action.platform,
      };
      return {
        ...state,
        accounts: state.accounts.map(item => {
          if (item.login == action.login) {
            return {...item, log: [log, ...item.log]};
          } else {
            return item;
          }
        }),
      };
    default:
      return state;
  }
};

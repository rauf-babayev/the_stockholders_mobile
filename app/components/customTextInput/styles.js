import {StyleSheet} from 'react-native';
import {dimensions} from '../../services/utils/constants';

export const styles = StyleSheet.create({
  mainContainer: {
    paddingTop: dimensions.AUTH_FORM_INPUT_PADDING_TOP,
  },
  formInput: {
    width: dimensions.FORM_TEXT_INPUT_W,
    height: dimensions.FORM_TEXT_INPUT_HEIGHT,
    marginBottom: dimensions.FORM_TEXT_INPUT_MARGIN_B,
    borderBottomWidth: dimensions.FORM_TEXT_INPUT_BORDER_W,
    paddingHorizontal: dimensions.FORM_TEXT_INPUT_PADDING_H,
    fontSize: dimensions.APP_MAIN_FONT_SIZE - 2,
  },
  formErrorIcon: {
    position: 'absolute',
    right: dimensions.FORM_TEXT_INPUT_ERROR_ICON_POSITION_MARGIN_RIGHT,
    top: dimensions.FORM_TEXT_INPUT_ERROR_ICON_POSITION_MARGIN_RIGHT,
  },
  textAnimContainer: {
    marginBottom: dimensions.CUSTOM_TEXT_INPUT_MARGIN_BOTTOM,
    position: 'absolute',
    zIndex: 2,
    left: dimensions.CUSTOM_TEXT_INPUT_CONTAINER_MARGINS,
    bottom: dimensions.CUSTOM_TEXT_INPUT_CONTAINER_MARGINS,
  },
  text: {
    fontSize: dimensions.APP_MAIN_FONT_SIZE,
  },
});

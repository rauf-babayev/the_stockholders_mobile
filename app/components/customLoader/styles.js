import {StyleSheet} from 'react-native';
import {dimensions} from './../../services/utils/constants';

export const styles = StyleSheet.create({
  mainContainer: {
    width: dimensions.LOADER_INDICATOR_WH,
    zIndex: 10,
    height: dimensions.LOADER_INDICATOR_WH,
    position: 'absolute',
    top: dimensions.HEADER_CONTAINER_H,
    left: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

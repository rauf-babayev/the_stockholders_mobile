import {strings} from '../../../services/utils/strings';

let initialState = {
  appTheme: strings.common.APP_LIGHT_THEME_TEXT,
  isDark: false,
  isAlert: false,
  alertText: '',
  deviceIp: '',
  isLoading: false,
  isRefreshing: false,
};

export const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case strings.actionTypes.SET_THEME:
      return {
        ...state,
        appTheme: action.theme,
        isDark: action.nextSwitchState,
      };
    case strings.actionTypes.ACTIVATE_ALERT:
      return {
        ...state,
        isAlert: true,
        alertText: action.text,
      };
    case strings.actionTypes.DEACTIVATE_ALERT:
      return {
        ...state,
        isAlert: false,
        alertText: '',
      };
    case strings.actionTypes.SET_DEVICE_IP:
      return {
        ...state,
        deviceIp: action.ip,
      };
    case strings.actionTypes.SET_LOADING:
      return {
        ...state,
        isLoading: action.isLoading,
      };
    case strings.actionTypes.SET_IS_REFRESHING:
      return {
        ...state,
        isRefreshing: action.isRefreshing,
      };
    default:
      return state;
  }
};

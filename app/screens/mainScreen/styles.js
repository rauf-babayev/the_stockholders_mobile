import {StyleSheet} from 'react-native';
import {dimensions} from '../../services/utils/constants';

export const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    paddingTop: dimensions.MAIN_SCREEN_PADDING_TOP,
  },
  commentsIconContainer: {
    flex: 0.2,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    paddingHorizontal: dimensions.MAIN_SCREEN_COMMENTS_CONTAINER_PADDING_H,
  },
  leadDescriptionContainer: {
    flex: 0.8,
    justifyContent: 'flex-start',
    padding: dimensions.MAIN_SCREEN_DESCRIPTION_CONTAINER_PADDING,
  },
  descriptionBox: {
    paddingBottom: dimensions.MAIN_SCREEN_DESCRIPTION_PADDING_BTM,
    height: dimensions.MAIN_SCREEN_DESCRIPTION_HEIGHT,
  },
  commentsText: {
    fontSize: dimensions.APP_MAIN_FONT_SIZE,
  },
  tooltipIcon: {
    position: 'absolute',
    bottom: 0,
    right: 0,
  },
  tooltipBtn: {
    width: dimensions.MAIN_SCREEN_COMMENTS_ICON_WH,
    height: dimensions.MAIN_SCREEN_COMMENTS_ICON_WH,
    justifyContent: 'center',
    alignItems: 'center',
  },
  tooltipText: {
    fontSize: dimensions.APP_MAIN_FONT_SIZE - 2,
    marginBottom: dimensions.MAIN_SCREEN_TOOLTIP_ICON_MARGIN_BTM,
  },
  anonymousIconContainer: {
    alignSelf: 'flex-end',
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: dimensions.MAIN_SCREEN_USER_PICTURE_MARGIN_BTM,
    padding: dimensions.MAIN_SCREEN_USER_PICTURE_PADDING,
    marginRight: dimensions.MAIN_SCREEN_USER_PICTURE_MARGIN_OUTSIDE_RIGHT,
    marginTop: dimensions.MAIN_SCREEN_USER_PICTURE_MARGIN_OUTSIDE_RIGHT,
  },
  mainScreenScroll: {
    paddingTop: 10,
    zIndex: 1,
  },
  toast: {
    paddingHorizontal: 15,
    paddingVertical: 5,
    borderRadius: 5,
    borderLeftWidth: 10,
    width: '90%',
  },
  topContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  logContainer: {
    flex: 0.5,
    padding: 10,
    alignItems: 'center',
  },
  logText: {
    fontSize: dimensions.LOG_TEXT_FONT_SIZE,
  },
  logShowBtn: {
    width: '80%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    position: 'absolute',
    bottom: 30,
  },
  pictureDeleteBtn: {
    position: 'absolute',
    zIndex: 10,
    right: 15,
    top: 15,
  },
  selectPictureBtn: {
    flexDirection: 'row',
    marginTop: 20,
  },
});

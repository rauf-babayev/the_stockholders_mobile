export const strings = {
  actionTypes: {
    AUTH_USER: 'AUTH_USER',
    SET_AUTH_USER: 'SET_AUTH_USER',
    NAVIGATE_TO_SCREEN: 'NAVIGATE_TO_SCREEN',
    SUBMIT_PASS_RECOVERY: 'SUBMIT_PASS_RECOVERY',
    LOGOUT_USER: 'LOGOUT_USER',
    SET_LOGOUT_USER: 'SET_LOGOUT_USER',
    CHANGE_THEME: 'CHANGE_THEME',
    SET_THEME: 'SET_THEME',
    ACTIVATE_ALERT: 'ACTIVATE_ALERT',
    DEACTIVATE_ALERT: 'DEACTIVATE_ALERT',
    SET_DEVICE_IP: 'SET_DEVICE_IP',
    SET_LOADING: 'SET_LOADING',
    SELECT_PICTURE_FROM_GALLERY: 'SELECT_PICTURE_FROM_GALLERY',
    SELECT_PICTURE_FROM_CAMERA: 'SELECT_PICTURE_FROM_CAMERA',
    SET_PROFILE_PICTURE: 'SET_PROFILE_PICTURE',
    DELETE_PROFILE_PICTURE: 'DELETE_PROFILE_PICTURE',
    FETCH_USER_COMMENTS: 'FETCH_USER_COMMENTS',
    SET_USER_COMMENTS: 'SET_USER_COMMENTS',
    SET_IS_REFRESHING: 'SET_IS_REFRESHING',
    SET_NEW_COMMENTS: 'SET_NEW_COMMENTS',
    SET_NEW_USER_LOG: 'SET_NEW_USER_LOG',
    SET_OLD_USER_LOG: 'SET_OLD_USER_LOG',
  },
  common: {
    AUTH_FORM_USERNAME_PLACEHOLDER: 'Login',
    AUTH_FORM_PASSWORD_PLACEHOLDER: 'Password',
    APP_NAME: 'DevEducation Leads',
    AUTH_FORM_SIGNIN_BTN_TEXT: 'Log In',
    AUTH_FORM_PASS_FORGET_BTN_TEXT: 'Forgot password?',
    TEXT_INPUT_ERROR_TEXT: 'Error',
    PASS_RECOVERY_SCREEN_TEXT: 'Enter your username',
    PASS_RECOVERY_SCREEN_BTN_TEXT: 'Submit',
    AUTH_FORM_USERNAME_INPUT_LABEL: 'Login',
    AUTH_FORM_PASSWORD_INPUT_LABEL: 'Password',
    CUSTOM_TEXT_INPUT_AUTO_CAPITALIZE_TYPE: 'none',
    APP_DARK_THEME_TEXT: 'dark',
    APP_LIGHT_THEME_TEXT: 'light',
    MODAL_WINDOW_ANIMATION_FADE_IN: 'fadeIn',
    MODAL_WINDOW_ANIMATION_FADE_OUT: 'fadeOut',
    MODAL_WINDOW_CLOSE_TEXT: 'OK',
    HEADER_CLOSE_BTN_TEXT: 'X',
    HEADER_LOGOUT_BTN_TEXT: 'Log Out',
    MAIN_SCREEN_COMMENTS_ICON_TEXT: 'Comments',
    LOADER_INDICATOR_SIZE: 'large',
    HEADER_LOGOUT_MODAL_TEXT: 'Are you sure you want to exit?',
    HEADER_LOGOUT_BACK_TEXT: 'X',
    LOG_DATE_TEXT: 'Date:',
    LOG_OS_TEXT: 'OS:',
    LOG_IP_TEXT: 'IP Address:',
    MODAL_PROMPT_ACCEPT_TEXT: 'Yes',
    MODAL_PROMPT_DECLINE_TEXT: 'No',
    MAIN_AUTH_LINK:
      'https://dev-eduaction-leads.herokuapp.com/checkLeadAuthorize',
    MAIN_AUTH_REQUEST_METHOD: 'POST',
    MAIN_AUTH_REQUEST_CONTENT_TYPE: 'application/json',
    PASS_RECOVERY_TOAST_TYPE: 'info',
    PASS_RECOVERY_TOAST_POSITION: 'top',
    PASS_RECOVERY_TOAST_FIRST_TEXT: 'Email was sent',
    PASS_RECOVERY_TOAST_SECOND_TEXT: 'You will recieve a response soon',
    SMTP_USERNAME: 'dev.education.leads@gmail.com',
    SMTP_PASSWORD: 'deveducationleads2020',
    SMTP_PORT: '465',
    SMTP_MAIL_HOST: 'smtp.gmail.com',
    EMAIL_SEND_SUCCESS_TEXT: 'Email was sent successfully',
    KEYBOARD_AVOIDING_BEHAVIOR: 'padding',
    KEYBOARD_AVOIDING_OS: 'ios',
    COMMENTS_SCREEN_NO_COMMENTS_TEXT: 'There are no comments for you yet',
    NET_INFO_TYPE_WIFI: 'wifi',
    NET_INFO_TYPE_CELLULAR: 'cellular',
    NEW_COMMENT_TOAST_FRIST_TEXT: 'New comment',
    NEW_COMMENT_TOAST_SECOND_TEXT: "Hey, looks like you've got new comment",
    PICTURE_DELETE_PROMPT_TEXT: 'Are you sure you want to delete your picture?',
    MORE_LOGS_BTN_TEXT: 'More logs...',
  },
  routes: {
    AUTH_STACK_LOGIN_ROUTE_NAME: 'Login',
    AUTH_STACK_PASS_RECOVERY_ROUTE_NAME: 'Pass Recovery',
    MAIN_STACK_MAIN_SCREEN_ROUTE: 'Main Screen',
    MAIN_STACK_COMMENTS_SCREEN_ROUTE: 'Comments Screen',
  },
  testIds: {
    AUTH_FORM_MAIN_BTN: 'auth_form_btn',
    AUTH_FORM_PASS_RECOVERY_BTN: 'auth_form_pass_recovery_btn',
    AUTH_FORM_MAIN_CONTAINER: 'auth_form_main_container',
    AUTH_FORM_USERNAME_INPUT: 'auth_form_username_input',
    AUTH_FORM_PASS_INPUT: 'auth_form_password_input',
    HEADER_SWITCH: 'header_switch',
    HEADER_CLOSE_LOGOUT_BTN: 'header_close_logout_btn',
    COMMENT_MODAL_WINDOW_BTN: 'comment_modal_window_btn',
    PASS_RECOVERY_FORM_INPUT: 'pass_recovery_form_input',
    PASS_RECOVERY_FORM_SUBMIT_BTN: 'pass_recovery_form_submit_btn',
    COMMENTS_SCREEN_SCROLL: 'comments_screen_scroll',
    MAIN_SCREEN_COMMENTS_ICON: 'main_screen_comments_icon',
  },
  error: {
    WRONG_LOGIN_AUTH_ERROR: 'Wrong login or password',
    COMMON_ERROR_AUTH_ERROR: 'Something went wrong',
    LENGTH_LOGIN_AUTH_ERROR: 'Login must be at least 3 characters',
    LENGTH_PASSWORD_AUTH_ERROR: 'Password must be at least 8 characters',
    EMPTY_LOGIN_AUTH_ERROR: 'Please enter your login',
    EMPTY_PASSWORD_EUTH_ERROR: 'Please enter your password',
    EMPTY_FORM_AUTH_ERROR: 'Please fill the form',
    PASS_RECOVERY_LOGIN_VALIDATION_ERROR: 'Wrong login',
    CONNECTION_LOST_ERROR: 'Connection is lost',
  },
};

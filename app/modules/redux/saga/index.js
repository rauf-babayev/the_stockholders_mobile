import {takeEvery, call, put, select} from 'redux-saga/effects';
import {strings} from '../../../services/utils/strings';
import {
  authUser,
  navigateToScreen,
  submitPassRecovery,
  fetchUserComments,
} from '../../functions';
import {
  setUserCommentsAction,
  setOldUserLogAction,
  setNewUserLogAction,
} from '../actions';

const accountsSelector = state => state.profileReducer.accounts;

export function* mainSagaWatcher() {
  yield takeEvery(strings.actionTypes.AUTH_USER, authWorker);
  yield takeEvery(strings.actionTypes.NAVIGATE_TO_SCREEN, navigationWorker);
  yield takeEvery(strings.actionTypes.CHANGE_THEME, changeThemeWorker);
  yield takeEvery(strings.actionTypes.LOGOUT_USER, logoutUserWorker);
  yield takeEvery(
    strings.actionTypes.SUBMIT_PASS_RECOVERY,
    submitPassRecoveryWorker,
  );
  yield takeEvery(
    strings.actionTypes.FETCH_USER_COMMENTS,
    fetchUserCommentsWorker,
  );
}

function* fetchUserCommentsWorker(action) {
  try {
    const data = yield call(fetchUserComments, action.login, action.pass);
    if (data) {
      yield put(setUserCommentsAction(data));
    }
  } catch (e) {
    yield put({
      type: strings.actionTypes.ACTIVATE_ALERT,
      text: e.message,
    });
    yield put({
      type: strings.actionTypes.SET_IS_REFRESHING,
      isRefreshing: false,
    });
  }
}

function* authWorker(action) {
  try {
    yield put({type: strings.actionTypes.SET_LOADING, isLoading: true});
    const result = yield call(authUser, action.userData);
    if (result) {
      yield put({type: strings.actionTypes.SET_AUTH_USER, data: result});
      const accounts = yield select(accountsSelector);
      const isRegistered = accounts.find(
        item => item.login == action.userData.login,
      );
      if (isRegistered) {
        yield put(
          setOldUserLogAction(
            action.userData.login,
            action.userData.ipAddress,
            action.userData.date,
            action.userData.platform,
          ),
        );
      } else {
        yield put(
          setNewUserLogAction(
            action.userData.login,
            action.userData.ipAddress,
            action.userData.date,
            action.userData.platform,
          ),
        );
      }
    } else {
      yield put({
        type: strings.actionTypes.ACTIVATE_ALERT,
        text: strings.error.WRONG_LOGIN_AUTH_ERROR,
      });
    }
    yield put({type: strings.actionTypes.SET_LOADING, isLoading: false});
  } catch (e) {
    yield put({
      type: strings.actionTypes.ACTIVATE_ALERT,
      text: e.message,
    });
    yield put({type: strings.actionTypes.SET_LOADING, isLoading: false});
  }
}

function* navigationWorker(action) {
  try {
    yield call(navigateToScreen, action.navigation);
  } catch (e) {}
}

function* submitPassRecoveryWorker(action) {
  try {
    const result = yield call(
      submitPassRecovery,
      action.username,
      action.setPassRecoveryState,
    );
    if (result) {
      yield put({
        type: strings.actionTypes.ACTIVATE_ALERT,
        text: strings.common.EMAIL_SEND_SUCCESS_TEXT,
      });
    } else {
      yield put({
        type: strings.actionTypes.ACTIVATE_ALERT,
        text: strings.error.COMMON_ERROR_AUTH_ERROR,
      });
    }
  } catch (e) {
    yield put({
      type: strings.actionTypes.ACTIVATE_ALERT,
      text: e.message,
    });
  }
}

function* logoutUserWorker() {
  yield put({type: strings.actionTypes.SET_LOGOUT_USER});
}

function* changeThemeWorker(action) {
  yield put({
    type: strings.actionTypes.SET_THEME,
    theme: action.theme,
    nextSwitchState: action.nextSwitchState,
  });
}

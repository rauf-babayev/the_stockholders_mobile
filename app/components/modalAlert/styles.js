import {StyleSheet} from 'react-native';
import {dimensions} from './../../services/utils/constants';

export const styles = StyleSheet.create({
  mainContainer: {
    width: dimensions.MODAL_WINDOW_WIDTH,
    alignSelf: 'center',
    padding: dimensions.MODAL_WINDOW_PADDING + 10,
    alignItems: 'center',
  },
  alertText: {
    fontSize: dimensions.APP_MAIN_FONT_SIZE,
    textAlign: 'center',
    lineHeight: dimensions.MODAL_WINDOW_COMMENT_TEXT_LINE_HEIGHT,
  },
  btnContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: dimensions.AUTH_BTN_PADDING_V,
    width: dimensions.MODAL_ALERT_BTN_WIDTH,
    marginTop: dimensions.AUTH_BTN_MARGIN_B + 20,
  },
  btnText: {
    fontSize: dimensions.APP_MAIN_FONT_SIZE,
  },
});

import {StyleSheet} from 'react-native';
import {dimensions} from '../../services/utils/constants';

export const styles = StyleSheet.create({
  formContainer: {
    width: dimensions.AUTH_FORM_INPUT_W,
    paddingHorizontal: dimensions.AUTH_FORM_INPUT_PADDING_H,
    paddingBottom: dimensions.AUTH_FORM_INPUT_PADDING_V,
    paddingTop: dimensions.AUTH_FORM_INPUT_PADDING_V - 20,
    maxHeight: dimensions.AUTH_FORM_CONTAINER_MAX_HEIGHT,
  },
  btnContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: dimensions.AUTH_BTN_PADDING_V,
    marginVertical: dimensions.AUTH_BTN_MARGIN_B,
  },
  btnText: {
    fontSize: dimensions.APP_MAIN_FONT_SIZE + 6,
  },
  forgotPassBtnContainer: {
    alignSelf: 'center',
  },
  forgotPassBtnText: {
    fontSize: dimensions.APP_MAIN_FONT_SIZE - 4,
    textDecorationLine: 'underline',
    fontWeight: dimensions.PASS_RECOVERY_TEXT_FONT_WEIGHT,
  },
});

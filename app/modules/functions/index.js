import RNSmtpMailer from 'react-native-smtp-mailer';
import Toast from 'react-native-toast-message';
import {strings} from '../../services/utils/strings';
import {dimensions} from '../../services/utils/constants';

export const authUser = async userData => {
  if (userData.login && userData.password) {
    if (
      userData.login.length >= dimensions.AUTH_REQUEST_LOGIN_MIN_LENGTH &&
      userData.password.length >= dimensions.AUTH_REQUEST_PASSWORD_MIN_LENGTH
    ) {
      if (!userData.login.match(/^(?=.*[a-zA-Z])[0-9a-zA-Z]{3,}$/)) {
        throw new Error(strings.error.WRONG_LOGIN_AUTH_ERROR);
      } else if (
        !userData.password.match(/^(?=.*\d)(?=.*[a-zA-Z])[0-9a-zA-Z]{8,}$/)
      ) {
        throw new Error(strings.error.WRONG_LOGIN_AUTH_ERROR);
      } else {
        const result = await fetch(strings.common.MAIN_AUTH_LINK, {
          method: strings.common.MAIN_AUTH_REQUEST_METHOD,
          headers: {
            'Content-Type': strings.common.MAIN_AUTH_REQUEST_CONTENT_TYPE,
          },
          body: JSON.stringify({
            login: userData.login,
            password: userData.password,
          }),
        })
          .then(response => response.json())
          .then(data => data)
          .catch(e => {
            throw new Error(strings.error.COMMON_ERROR_AUTH_ERROR);
          });
        return result;
      }
    } else {
      if (userData.login.length < dimensions.AUTH_REQUEST_LOGIN_MIN_LENGTH) {
        throw new Error(strings.error.LENGTH_LOGIN_AUTH_ERROR);
      } else if (
        userData.password.length < dimensions.AUTH_REQUEST_PASSWORD_MIN_LENGTH
      ) {
        throw new Error(strings.error.LENGTH_PASSWORD_AUTH_ERROR);
      }
    }
  } else {
    if (!userData.login && userData.password) {
      throw new Error(strings.error.EMPTY_LOGIN_AUTH_ERROR);
    } else if (userData.login && !userData.password) {
      throw new Error(strings.error.EMPTY_PASSWORD_EUTH_ERROR);
    } else if (!userData.login && !userData.password) {
      throw new Error(strings.error.EMPTY_FORM_AUTH_ERROR);
    }
  }
};

export const navigateToScreen = navigation => {
  navigation();
};

export const fetchUserComments = async (login, pass) => {
  const result = await fetch(strings.common.MAIN_AUTH_LINK, {
    method: strings.common.MAIN_AUTH_REQUEST_METHOD,
    headers: {
      'Content-Type': strings.common.MAIN_AUTH_REQUEST_CONTENT_TYPE,
    },
    body: JSON.stringify({
      login: login,
      password: pass,
    }),
  })
    .then(response => response.json())
    .then(data => data)
    .catch(e => console.log(e));
  return result;
};

export const submitPassRecovery = async (username, setPassRecoveryState) => {
  if (username) {
    if (username.length >= dimensions.AUTH_REQUEST_LOGIN_MIN_LENGTH) {
      if (!username.match(/^(?=.*[a-zA-Z])[0-9a-zA-Z]{3,}$/)) {
        throw new Error(strings.error.PASS_RECOVERY_LOGIN_VALIDATION_ERROR);
      } else {
        Toast.show({
          type: strings.common.PASS_RECOVERY_TOAST_TYPE,
          position: strings.common.PASS_RECOVERY_TOAST_POSITION,
          text1: strings.common.PASS_RECOVERY_TOAST_FIRST_TEXT,
          text2: strings.common.PASS_RECOVERY_TOAST_SECOND_TEXT,
          visibilityTime: dimensions.PASS_RECOVERY_TOAST_VISIBILITY_TIME,
          autoHide: true,
          topOffset: dimensions.PASS_RECOVERY_TOAST_TOP_OFFSET,
          bottomOffset: dimensions.PASS_RECOVERY_TOAST_BOTTOM_OFFSET,
        });
        setPassRecoveryState({login: ''});
        try {
          await sendEmail(
            'Password recovery',
            `<h1>Password recovery request</h1>
            <p>Hello,</p>
            <p>We recieved a request for password reset from <b>${username}</b></p>
            <p> - Dev Education Leads</p>`,
            'leads250790@gmail.com',
          );
        } catch (e) {
          throw new Error(strings.error.COMMON_ERROR_AUTH_ERROR);
        }
      }
    } else {
      if (username.length < dimensions.AUTH_REQUEST_LOGIN_MIN_LENGTH) {
        throw new Error(strings.error.LENGTH_LOGIN_AUTH_ERROR);
      }
    }
  } else {
    if (!username) {
      throw new Error(strings.error.EMPTY_LOGIN_AUTH_ERROR);
    }
  }
};

export const dimensionSetter = (
  screenWidth,
  dimension,
  decreaseBy,
  increaseBy,
) => {
  switch (true) {
    case screenWidth < 390: {
      return dimension - decreaseBy;
    }
    case screenWidth < 800: {
      return dimension;
    }
    default: {
      return dimension + increaseBy;
    }
  }
};

export const getFullDate = date => {
  let hours = date.getHours() < 10 ? `0${date.getHours()}` : date.getHours();
  let minutes =
    date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes();
  let day = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
  let month =
    date.getMonth() + 1 < 10 ? `0${date.getMonth() + 1}` : date.getMonth() + 1;
  let year = date.getFullYear();
  return `${hours}:${minutes} - ${day}/${month}/${year}`;
};

export const customTimeout = (callback, delay) => {
  return setTimeout(() => {
    callback();
    customTimeout(callback, delay);
  }, delay);
};

const sendEmail = async (subject, text, recipient) => {
  await RNSmtpMailer.sendMail({
    mailhost: strings.common.SMTP_MAIL_HOST,
    port: strings.common.SMTP_PORT,
    ssl: true,
    username: strings.common.SMTP_USERNAME,
    password: strings.common.SMTP_PASSWORD,
    from: strings.common.SMTP_USERNAME,
    recipients: `${recipient}`,
    bcc: [],
    subject: `${subject}`,
    htmlBody: `${text}`,
    attachmentPaths: [],
    attachmentNames: [],
    attachmentTypes: [],
  })
    .then(response => {
      console.log(response);
      return true;
    })
    .catch(err => {
      console.log(err);
      return err;
    });
};

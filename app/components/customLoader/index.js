import React from 'react';
import {View, ActivityIndicator} from 'react-native';
import {styles} from './styles';
import {colors} from './../../services/utils/colors';
import {useSelector} from 'react-redux';
import {dimensions} from '../../services/utils/constants';
import {strings} from '../../services/utils/strings';

const CustomLoader = () => {
  const theme = useSelector(state => state.appReducer.appTheme);

  return (
    <View
      style={[
        styles.mainContainer,
        {backgroundColor: colors[`${theme}`].LOADER_BG},
      ]}>
      <ActivityIndicator
        style={{transform: [{scale: dimensions.LOADER_SCALE}]}}
        size={strings.common.LOADER_INDICATOR_SIZE}
        color={colors[`${theme}`].LOADER_COLOR}
      />
    </View>
  );
};

export default CustomLoader;

import {StyleSheet} from 'react-native';
import {dimensions} from '../../services/utils/constants';

export const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mainImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: dimensions.SPLASH_SCREEN_WH,
    height: dimensions.SPLASH_SCREEN_WH,
  },
});

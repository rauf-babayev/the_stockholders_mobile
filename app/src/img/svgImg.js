import React from 'react';
import Svg, {Path, G, Rect} from 'react-native-svg';

export const SvgImg = ({paths, iconW, iconH, style}) => {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width={iconW}
      height={iconH}
      viewBox="0 0 378 56"
      fill="none"
      style={{...style}}>
      {paths.map((item, index) => {
        return <Path d={item.d} fill={item.fill} key={index} />;
      })}
    </Svg>
  );
};

export const SvgErrorImg = ({paths, iconW, iconH, styles, bgColor}) => {
  return (
    <Svg
      viewBox={'0 0 365.71733 365'}
      xmlns={'http://www.w3.org/2000/svg'}
      width={iconW}
      height={iconH}
      style={{...styles}}>
      <G fill={bgColor}>
        {paths.map((item, index) => {
          return <Path d={item} key={index} />;
        })}
      </G>
    </Svg>
  );
};

export const DefaultSvgIcon = ({paths, iconW, iconH, styles, bgColor}) => {
  return (
    <Svg
      viewBox={'0 0 512 512'}
      xmlns={'http://www.w3.org/2000/svg'}
      width={iconW}
      height={iconH}
      style={{...styles}}>
      {paths.map((item, index) => {
        return <Path d={item} key={index} fill={bgColor} />;
      })}
    </Svg>
  );
};

export const AnonymousUserIcon = ({paths, iconW, iconH, styles}) => {
  return (
    <Svg
      viewBox={'0 0 480 480'}
      xmlns={'http://www.w3.org/2000/svg'}
      width={iconW}
      height={iconH}
      style={{...styles}}>
      {paths.map((item, index) => {
        return <Path d={item} key={index} fill={'white'} />;
      })}
    </Svg>
  );
};

export const BWIconSun = ({theme, style, iconWH}) => {
  return (
    <Svg
      style={style}
      xmlns="http://www.w3.org/2000/svg"
      width={iconWH}
      height={iconWH}
      viewBox="0 0 512 512"
      fill={theme == 'dark' ? 'white' : 'white'}>
      <Path d="M256 360.099c-57.4 0-104.099-46.698-104.099-104.099S198.6 151.901 256 151.901 360.099 198.6 360.099 256 313.4 360.099 256 360.099zm0-178.198c-40.858 0-74.099 33.241-74.099 74.099s33.24 74.099 74.099 74.099 74.099-33.241 74.099-74.099-33.241-74.099-74.099-74.099zM256 116.071c-8.284 0-15-6.716-15-15V15c0-8.284 6.716-15 15-15s15 6.716 15 15v86.071c0 8.284-6.716 15-15 15zM146.449 161.449a14.95 14.95 0 01-10.606-4.393L74.98 96.194c-5.858-5.858-5.858-15.355 0-21.213 5.857-5.858 15.355-5.858 21.213 0l60.862 60.861c5.858 5.858 5.858 15.355 0 21.213a14.95 14.95 0 01-10.606 4.394zM101.071 271H15c-8.284 0-15-6.716-15-15s6.716-15 15-15h86.071c8.284 0 15 6.716 15 15s-6.716 15-15 15zM85.587 441.413a14.946 14.946 0 01-10.606-4.394c-5.858-5.858-5.858-15.355 0-21.213l60.862-60.861c5.858-5.857 15.356-5.857 21.213 0 5.858 5.858 5.858 15.355 0 21.213L96.193 437.02a14.953 14.953 0 01-10.606 4.393zM256 512c-8.284 0-15-6.716-15-15v-86.071c0-8.284 6.716-15 15-15s15 6.716 15 15V497c0 8.284-6.716 15-15 15zM426.413 441.413a14.95 14.95 0 01-10.606-4.393l-60.862-60.861c-5.858-5.858-5.858-15.355 0-21.213 5.857-5.858 15.355-5.858 21.213 0l60.862 60.861c5.858 5.858 5.858 15.355 0 21.213a14.957 14.957 0 01-10.607 4.393zM497 271h-86.071c-8.284 0-15-6.716-15-15s6.716-15 15-15H497c8.284 0 15 6.716 15 15s-6.716 15-15 15zM365.551 161.449a14.946 14.946 0 01-10.606-4.394c-5.858-5.858-5.858-15.355 0-21.213l60.862-60.861c5.857-5.856 15.355-5.858 21.213 0s5.858 15.355 0 21.213l-60.862 60.861a14.956 14.956 0 01-10.607 4.394z" />
    </Svg>
  );
};

export const BWIconMoon = ({theme, style, iconWH}) => {
  return (
    <Svg
      style={style}
      width={iconWH}
      height={iconWH}
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 512 512"
      fill={theme == 'dark' ? 'white' : 'black'}>
      <Path d="M507.681 209.011a15.001 15.001 0 00-14.433-12.262 14.966 14.966 0 00-14.936 11.642c-15.26 66.498-73.643 112.941-141.978 112.941-80.321 0-145.667-65.346-145.667-145.666 0-68.335 46.443-126.718 112.942-141.976a15.002 15.002 0 0011.643-14.934 15.002 15.002 0 00-12.259-14.434A258.098 258.098 0 00256 0C187.62 0 123.333 26.629 74.98 74.981 26.629 123.333 0 187.62 0 256s26.629 132.667 74.98 181.019C123.333 485.371 187.62 512 256 512s132.667-26.629 181.02-74.981C485.371 388.667 512 324.38 512 256c0-15.722-1.453-31.531-4.319-46.989zM256 482C131.383 482 30 380.617 30 256c0-118.227 91.264-215.544 207.036-225.212a176.343 176.343 0 00-37.513 34.681c-25.058 31.071-38.857 70.207-38.857 110.197 0 96.863 78.804 175.666 175.667 175.666 39.99 0 79.126-13.8 110.197-38.857a176.387 176.387 0 0034.682-37.511C471.544 390.736 374.228 482 256 482z" />
    </Svg>
  );
};

export const CameraIconSvg = ({paths, iconW, iconH, styles, bgColor}) => {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 34.39 34.39"
      width={iconW}
      height={iconH}
      style={{...styles}}>
      <Rect x="3.451" y="3.259" width="7.051" height="2.559" />
      {paths.map((item, index) => {
        return <Path d={item} key={index} fill={'white'} />;
      })}
    </Svg>
  );
};

export const BackIcon = ({paths, iconW, iconH, styles, bgColor}) => {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 240.823 240.823"
      width={iconW}
      height={iconH}>
      {paths.map((item, index) => {
        return <Path d={item} key={index} fill={bgColor} />;
      })}
    </Svg>
  );
};

export const LogoutSvgIcon = ({paths, iconW, iconH, styles, bgColor}) => {
  return (
    <Svg
      viewBox={'0 0 330 330'}
      xmlns={'http://www.w3.org/2000/svg'}
      width={iconW}
      height={iconH}
      style={{...styles}}>
      {paths.map((item, index) => {
        return <Path strokeWidth={10} d={item} key={index} fill={bgColor} />;
      })}
    </Svg>
  );
};
